package gauntlet;

import java.io.Serializable;

enum MessageType {
    JOIN, LOBBY, INPUT, PLAYER, ENEMY, TILE, GAMEOVER;
}

abstract class Message implements Serializable {
    private MessageType type;

    public Message(MessageType type) {
        this.type = type;
    }

    public MessageType getType() {
        return type;
    }
}
