package gauntlet;

import org.newdawn.slick.Animation;

import jig.ResourceManager;
import jig.Vector;

public class Adventurer extends Player {
    private static final int HEALTH = 200;
    private static final int ATTACK = 20;
    private static final double DEFENSE = 0.05;
    private static final int SPEED = 4;
    private static final int ATTACK_SPEED = 3;

    public Adventurer(Vector position) {
        this(position, true);
    }

    public Adventurer(Vector position, boolean selected) {
        super(position, HEALTH, ATTACK, DEFENSE, SPEED, ATTACK_SPEED, playerType.ADVENTURER, selected);

        right_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 0, 12, 0, true, 150, true);
        right_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 1, 7, 1, true, 100, true);
        right_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 3, 9, 3, true, 50, true);
        right_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 6, 3, 6, true, 100, true);
        right_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 7, 6, 7, true, 100, true);

        left_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 8, 12, 8, true, 150, true);
        left_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 9, 7, 9, true, 100, true);
        left_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 11, 9, 11, true, 50, true);
        left_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 14, 3, 14, true, 100, true);
        left_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ADVENTURER, 64, 64), 0, 15, 6, 15, true, 100, true);


        right_damage.setLooping(false);
        left_damage.setLooping(false);
        right_damage.stop();
        left_damage.stop();

        right_death.setLooping(false);
        left_death.setLooping(false);
        right_death.stop();
        left_death.stop();
        
        right_hurt.setLooping(false);
        left_hurt.setLooping(false);
        right_hurt.stop();
        left_hurt.stop();

        current = right_idle;
        addAnimation(current);
    }
}
