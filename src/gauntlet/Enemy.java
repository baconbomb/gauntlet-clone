package gauntlet;

import org.newdawn.slick.Animation;

import jig.ConvexPolygon;
import jig.Entity;
import jig.Vector;

enum EnemyType {
    SKELETON, SNAKE, GOLEM;
}
public abstract class Enemy extends Entity {

    private int health, attack, attack_speed, speed, damageCooldown, floor, attackTimer, deathTimer;
    private boolean right, idle, dead;
    private Vector prevPosition;
    private ConvexPolygon weaponHitbox;
    public Animation right_idle, right_walk, right_damage, right_hurt, right_death;
    public Animation current, left_idle, left_walk, left_damage, left_hurt, left_death;


    public Enemy(Vector position, int health, int attack, int attack_speed, int speed, int floor) {
        super(position);
        this.health = health;
        this.attack = attack;
        this.attack_speed = attack_speed;
        this.speed = speed;
        this.floor = floor;

        damageCooldown = attackTimer = deathTimer = 0;
        right = true;
        idle = true;
        dead = false;

        addShape(new ConvexPolygon(8,8),new Vector (0,25)); // hit box near feet
        weaponHitbox = new ConvexPolygon(16,32);
    }


    /** Returns player's previous position **/
    public Vector getPrevPosition() {
        return prevPosition;
    }


    /** Return enemy attack value **/
    public int getAttack() {
        return attack;
    }

    /** Moves enemy left **/
    public void moveLeft() {
        right = false;
        idle = false;
        updateAnimation(right_walk, left_walk);
        translate(-speed,0);
    }

    /** Moves enemy right **/
    public void moveRight() {
        right = true;
        idle = false;
        updateAnimation(right_walk, left_walk);
        translate(speed,0);
    }

    /** Moves enemy up **/
    public void moveUp() {
        idle = false;
        updateAnimation(right_walk, left_walk);
        translate(0,-speed);
    }

    /** Moves enemy down **/
    public void moveDown() {
        idle = false;
        updateAnimation(right_walk, left_walk);
        translate(0,speed);
    }

    /** Moves enemy back when collision occurs **/
    public void moveBack() {
        setPosition(getPrevPosition());
    }

    /** Checks if enemy is currently attacking **/
    public boolean isAttacking() {
        if (right_damage.isStopped() == false || left_damage.isStopped() == false) {
            return true;
        }
        return false;
    }

    /** Used to attack players **/
    public void attack() {
        if (attackTimer <= 0) {
            int x;
            if (right == true)
                x = 16;
            else
                x = -16;

            idle = false;
            attackTimer = (int) (3000 / attack_speed);
            addShape(weaponHitbox, new Vector(x,8));
            updateAnimation(right_damage, left_damage);
        }
    }

    /** Returns enemy's current health **/
    public int getHealth() {
        return health;
    }

    /** Update enemy's health **/
    public void setHealth(int health) {
        this.health = health;
    }

    /** Decrements health by a variable amount **/
    public boolean removeHealth(int damage) {
        if(!dead) {
	    	if (damageCooldown <= 0) {
	            health -= damage;
	
	            if (health <= 0) { // enemy is dead
	                death();
	            }
	            else {
	                damageCooldown = 1000;
	                hurt();
	            }
	            return true;
	        }
        }
        return false;
    }

    /** Displays idle animation **/
    public void idle() {
        updateAnimation(right_idle, left_idle);
    }

    /** Displays hurt animation **/
    public void hurt() {
        updateAnimation(right_hurt, left_hurt);
    }
    
    /** Checks if hurt animation is currently playing **/
    public boolean isHurt() {
        if (right_hurt.isStopped() == false || left_hurt.isStopped() == false) {
            return true;
        }
        return false;
    }

    /** Displays death animation **/
    public void death() {
        dead = true;
        deathTimer = 3500;
        attackTimer = 4000;
        damageCooldown = 4000;
        updateAnimation(right_death, left_death);
    }
    
    /** Checks if death animation is currently playing **/
    public boolean isDeadAnimation() {
        if (right_death.isStopped() == false || left_death.isStopped() == false) {
            return true;
        }
        return false;
    }

    public boolean isDead() {
        if (dead == true && deathTimer <= 0)
            return true;
        else
            return false;
    }

    /** Get the current floor that the player is on **/
    public int getFloor() {
        return floor;
    }

    /** Updates enemy animation **/
    public void updateAnimation(Animation r, Animation l) {
        if (isAttacking() == false && isDeadAnimation() == false && isHurt() == false) {
            removeAnimation(current);
            current = right ? r : l;
            addAnimation(current);
        }
        	
        // enemy is dead
        if (dead == true) {
            if (isAttacking() == true) { // prevents enemy from getting stuck in attack animation
                removeAnimation(current);
                current = right ? r : l;
                addAnimation(current);
            }
            current.setDuration(current.getFrameCount()-1, 2500); 
         }

        if (current.isStopped() == true)
            current.restart();
    }

    public void update(int delta, Player[] players) {
        if (dead == true) {
            if (deathTimer > 0)
                deathTimer -= delta;
            return;
        }

        if (right_damage.isStopped() == true && left_damage.isStopped() == true) {
            if (right_death.isStopped() == true && left_death.isStopped() == true) {
                removeShape(weaponHitbox);
                if (idle == true)
                    idle();
                else
                    idle = true;
            }
        }

        if (damageCooldown > 0)
            damageCooldown -= delta;

        if (attackTimer > 0)
            attackTimer -= delta;

        prevPosition = getPosition();

        // Used to find the closest player to an enemy
        float min = Float.MAX_VALUE;
        Player closest = null;
        for (Player p : players) {
            float temp = p.getPosition().distance(this.getPosition());
            if (temp < min) {
                min = temp;
                closest = p;
            }
        }
        // Enemy moves towards the closest player
        if (min > 30) {
            Vector v = closest.getPosition().subtract(this.getPosition());
            if (Math.abs(v.getX()) > Math.abs(v.getY())) {
                if (v.getX() > 0)
                    moveRight();
                if (v.getX() < 0)
                    moveLeft();
            }
            else {
                if (v.getY() > 0)
                    moveDown();
                if (v.getY() < 0)
                    moveUp();
            }
        }
        else
            attack();
    }

}
