package gauntlet;

import jig.Vector;

public class PlayerMessage extends Message {
    private int playerID;
    private int health;
    private int floor;
    private int keys;
    private Vector position;
    private boolean right;
    private boolean up;
    private int score;

    public PlayerMessage(int id, Player p) {
        super(MessageType.PLAYER);
        playerID = id;
        health = p.getHealth();
        position = p.getPosition();
        floor = p.getFloor();
        right = p.isRight();
        up = p.isUp();
        keys = p.getKeyCount();
        score = p.getScore();
    }

    public int getPlayerID() {
        return playerID;
    }

    public Vector getPosition() {
        return position;
    }

    public int getHealth() {
        return health;
    }

    public void updatePlayer(Player[] player) {
        //System.out.println("player" + playerID + " at " + position);
        player[playerID].setPosition(position);
        player[playerID].setHealth(health);
        player[playerID].setFloor(floor);
        player[playerID].setRight(right);
        player[playerID].setUp(up);
        player[playerID].setKeyCount(keys);
        player[playerID].setScore(score);
    }
}
