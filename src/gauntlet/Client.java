package gauntlet;

import java.net.*;
import java.io.*;
import java.util.LinkedList;
import java.util.Queue;

public class Client extends Thread {
    private static final int HOST_PORT = 4445;
    private static final int CLIENT_PORT = 4446;
    private static String SERVER_IP;
    private static InetAddress[] addresses = new InetAddress[3];
    private static IPTuple[] table = new IPTuple[3];
    private int port;
    private boolean running;
    private Queue<Message> msgInQueue;
    private Queue<Message> msgOutQueue;

    private DatagramSocket socket;

    public static void setServerIP(String ip) {
        SERVER_IP = ip;
    }

    public Client() {
        msgInQueue = Gauntlet.msgInQueue;
        msgOutQueue = Gauntlet.msgOutQueue;
        try {
            socket = new DatagramSocket();
        }
        catch(IOException i) {
            System.out.println(i);
        }
        running = true;
        if (Gauntlet.isHost == true)
            port = CLIENT_PORT;
        else
            port = HOST_PORT;
        //addresses = new InetAddress[3];
    }

    public static void addEntry(InetAddress a, int p) {
        for (int i = 0; i < table.length; i++) {
            if (table[i] == null)
                table[i] = new IPTuple(a, p);
            else if (table[i].equals(a))
                break;
        }
    }

    public static void addAddress(InetAddress addr) {
        for (int i = 0; i < addresses.length; i++) {
            if (addresses[i] == null) {
                addresses[i] = addr;
                break;
            }
            else if (addresses[i].equals(addr))
                break;
        }
    }

    public void run() {
        try {
            InetAddress serverAddress = InetAddress.getByName(SERVER_IP);
            while (msgOutQueue.peek() != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                Message msg = msgOutQueue.poll();
                if (msg == null) continue;
                if (msg.getType() == MessageType.PLAYER) {
                    PlayerMessage pMsg = (PlayerMessage) msg;
                    //System.out.println("sending msg for p" + pMsg.getPlayerID() + " at " + pMsg.getPosition());
                }
                oos.writeObject(msg);
                oos.flush();
                byte[] buf = baos.toByteArray();
                //System.out.println(buf.length + "bytes");
                if (Gauntlet.isHost == false) {
                    DatagramPacket packet = new DatagramPacket(buf, buf.length, serverAddress, port);
                    socket.send(packet);
                }
                else {
                    //for (IPTuple entry : table) {
                    //    if (entry != null) {
                    //        InetAddress addr = entry.address;
                    //        port = entry.port;
                    //        DatagramPacket packet = new DatagramPacket(buf, buf.length, addr, port);
                    //        socket.send(packet);
                    //    }
                    //}
                    for (InetAddress addr : addresses) {
                        if (addr != null) {
                            DatagramPacket packet = new DatagramPacket(buf, buf.length, addr, port);
                            socket.send(packet);
                        }
                    }
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        socket.close();
    }

    public static class IPTuple {
        InetAddress address;
        int port;

        public IPTuple(InetAddress a, int p) {
            address = a;
            port = p;
        }
    }
}
