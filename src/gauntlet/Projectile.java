package gauntlet;

import jig.ConvexPolygon;
import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

import org.newdawn.slick.Animation;


public class Projectile extends Entity {
    private static final int speed = 4;

    private Vector velocity;
    private Animation animation, right_red_start, right_red, left_red_start, left_red, right_blue_start, right_blue, left_blue_start, left_blue;
    private boolean right;
    private boolean red;
    private int ttl; // time in ms before projectile expires
    private int attack;
    private int floor;

    public Projectile(Vector position, boolean right, boolean red, int attack, int floor) {
        super(position);
        this.right = right;
        this.red = red;
        this.attack = attack;
        this.ttl = 1500;
        this.floor = floor;
        addShape(new ConvexPolygon(8, 8));

        right_red_start  = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 0, 2, 0, true, 150, true);
        right_red        = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 1, 3, 1, true, 150, true);
        left_red_start   = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 2, 2, 2, true, 150, true);
        left_red         = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 3, 3, 3, true, 150, true);
        right_blue_start = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 4, 2, 4, true, 150, true);
        right_blue       = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 5, 3, 5, true, 150, true);
        left_blue_start  = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 6, 2, 6, true, 150, true);
        left_blue        = new Animation(ResourceManager.getSpriteSheet(Gauntlet.PROJECTILE, 32, 32), 0, 7, 3, 7, true, 150, true);
        right_red_start.setLooping(false);
        right_blue_start.setLooping(false);
        left_red_start.setLooping(false);
        left_blue_start.setLooping(false);

        if (right == true) {
            this.translate(32, 0);
            velocity = new Vector(speed, 0);
            if (red == true)
                animation = right_red_start;
            else
                animation = right_blue_start;
        }
        else {
            this.translate(-32, 0);
            velocity = new Vector(-speed, 0);
            if (red == true)
                animation = left_red_start;
            else
                animation = left_blue_start;
        }
        addAnimation(animation);
    }

    public int getAttack() {
        ttl = 0;
        return attack;
    }

    public boolean isDead() {
        if (ttl > 0)
            return false;
        else
            return true;
    }

    public void update(int delta) {
        ttl -= delta;
        if (animation.isStopped() == true) {
            removeAnimation(animation);
            if (right == true) {
                if (red == true)
                    animation = right_red;
                else
                    animation = right_blue;
            }
            else {
                if (red == true)
                    animation = left_red;
                else
                    animation = left_blue;
            }
            addAnimation(animation);
        }
        translate(velocity);
    }
    
    public int getFloor() {
        return floor;
    }
}
