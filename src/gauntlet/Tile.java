package gauntlet;

import jig.Collision;
import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

import java.util.Random;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;

/** 
 * Tile is an entity that is used by the Map class.
 * @author Paige, Chris
 *
 */

public class Tile extends Entity {
    private static final int NUM_SPRITES = 23;
    private static final int NUM_ITEMS = 18;
    private static final int TILE_WIDTH = 32;
    private static final int TILE_HEIGHT = 32;
    private static final int CHAR_OFFSET = 32;

    private Image tileImage; // stores the current image of the tile
    private Image itemImage; // stores the current image of the item
    private Animation itemAnimation; // stores the current animation of the item
    private Animation bubbleAnimation; // an animation used with chest tile
    private Animation potionAnimation; // an animation used with chest tile
    private Random rand; // used to select a random potion from sprite sheet
    private int r; // potion selected from rand
    private int sprite; // stores the current sprite index
    private int item; // index for item stored in this tile
    private boolean isLocked;
    private int foodHealth = 5;

    public Tile(int x, int y, int item, int sprite) {
        super(x, y);
        this.sprite = sprite;
        this.item = item;
        isLocked = true;
        tileImage = ResourceManager.getImage(Gauntlet.DUNGEON).getSubImage(sprite*TILE_WIDTH, 0, 32, 32);
        
        addImageWithBoundingBox(tileImage);
        if (item > -1) {
            if (item == 0) { // chest
            		rand = new Random();
            		r = rand.nextInt(12-8) + 8;
            		
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 4, item, true, 100, false);
                bubbleAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.BUBBLE, 32, 32), 0, 0, 3, 0, true, 50, false);
                potionAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, r, 7, r, true, 50, false);
                addAnimation(itemAnimation);
                itemAnimation.stop();
                itemAnimation.setLooping(false);
                itemAnimation.setPingPong(true);
                itemAnimation.setDuration(4, 1500); // stay open for longer
                bubbleAnimation.stop();
                bubbleAnimation.setLooping(false);
                bubbleAnimation.setPingPong(true);
                bubbleAnimation.setDuration(3, 2000);
                potionAnimation.stop();
                
            }
            else if (item == 2) { // door
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 3, item, true, 150, false);
                addAnimation(itemAnimation);
                itemAnimation.stop();
                itemAnimation.setLooping(false);
            }
            else if (item == 6) { // spike trap
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 2, item, true, 50, true);
                addAnimation(itemAnimation);
                itemAnimation.setPingPong(true);
                itemAnimation.setDuration(0, 2000); // stay down longer
                itemAnimation.setDuration(2, 1000); // stay up a bit longer
            }
            else if (item > 7 && item < 12) { // potions
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 7, item, true, 50, true);
                addAnimation(itemAnimation);
            }
            else if (item == 12) { // torch
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 4, item, true, 150, true);
                addAnimation(itemAnimation);
            }
            else if (item == 17) { // golden door
            		itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 3, item, true, 150, false);
                addAnimation(itemAnimation);
                itemAnimation.stop();
                itemAnimation.setLooping(false);
            }
            else { // non-animated items
                itemImage = ResourceManager.getImage(Gauntlet.ITEMS).getSubImage(0, item*TILE_HEIGHT, 32, 32);
                addImage(itemImage);
            }
        }
        else
            itemImage = null;
    }

    public Tile(int x, int y) {
        this(x, y, 0, 0);
    }

    /** constructor using 16 bit char */
    public Tile(int x, int y, char c) {
        this(x, y, c/256 - CHAR_OFFSET-1, c%256 - CHAR_OFFSET);
    }

    /** convert the item and sprite into a 16 bit char */
    public char toChar() {
        char c = (char) ((item + CHAR_OFFSET+1) * 256 + sprite + CHAR_OFFSET);
        return c;
    }

    /** returns the index of the sprite for the tile */
    public int getSprite() {
        return sprite;
    }

    /** Used to traverse left through the dungeon sprite sheet */
    public void prevSprite() {
        setTileSprite(sprite-1);
    }

    /** Used to traverse right through the dungeon sprite sheet */
    public void nextSprite() {
        setTileSprite(sprite+1);
    }

    /** returns the index of the item for the tile */
    public int getItem() {
        return item;
    }

    /** Used to traverse up through the item sprite sheet */
    public void prevItem() {
        setTileItem(item-1);
    }

    /** Used to traverse down through the item sprite sheet */
    public void nextItem() {
        setTileItem(item+1);
    }

    /** Used when updating a item on the map */
    public void setTileItem(int i) {
        if (i < -1)
            i = -1;
        if (i > NUM_ITEMS)
            i = NUM_ITEMS;
        item = i;
        removeImage(itemImage);
        removeAnimation(itemAnimation);
        if (i > -1) {
            if (i == 6) {
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, i, 2, i, true, 250, true);
                addAnimation(itemAnimation);
            }
            else if (i > 7 && i < 12) {
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, i, 7, i, true, 150, true);
                addAnimation(itemAnimation);
            }
            else if (i == 12) {
                itemAnimation = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, i, 4, i, true, 150, true);
                addAnimation(itemAnimation);
            }
            else {
                itemImage = ResourceManager.getImage(Gauntlet.ITEMS).getSubImage(0, i*TILE_HEIGHT, 32, 32);
                addImage(itemImage);
            }
        }
    }

    /** Set tile sprite */
    public void setTileSprite(int i) {
        if (i < 0)
            i = 0;
        if (i > NUM_SPRITES)
            i = NUM_SPRITES;
        sprite = i;
        removeImage(tileImage);
        tileImage = ResourceManager.getImage(Gauntlet.DUNGEON).getSubImage(i*TILE_WIDTH, 0, 32, 32);
        addImage(tileImage);
    }

    /** Updates tile and player when a player collides with a tile */
    public void handleCollision(Player p) {
        Collision c = collides(p);
        if (c == null)
            return;
        switch (item) { // item tile
            case 0: // chest
                if (isLocked == true) { // opens if player has key and room in inventory
                    if (p.getKeyCount() > 0 && p.addPotion(r) == true) {
                        p.removeKey(1);
                        isLocked = false;
                        itemAnimation.start();
                        addAnimation(bubbleAnimation,new Vector(0,-32));
                        bubbleAnimation.start();
                    }
                }
                else if (itemAnimation.isStopped())
                    itemAnimation.start();
                p.moveBack();
                break;
            case 1: // crates
                p.moveBack();
                break;
            case 2: // door
                if (isLocked == true) {
                    if (p.getKeyCount() > 0) {
                        p.removeKey(1);
                        isLocked = false;
                        itemAnimation.start();
                    }
                }
                else if (itemAnimation.isStopped())
                    item = -1;
                p.moveBack();
                break;
            case 3: // food
                if (p.getHealth() < p.getMaxHealth()) {
                    p.addHealth(foodHealth);
                    removeImage(itemImage);
                    item = -1;
                }
                break;
            case 5: // key
                p.addtKeyCount(1);
                removeImage(itemImage);
                item = -1;
                break;
            case 6: // trap
                if (itemAnimation.getFrame() != 0) {
                    p.removeHealth(5);
                    p.hurt();
                }
                break;
            case 8: //red potion
            case 9: //yellow potion
            case 10: //green potion
            case 11: //blue potion
                if (p.addPotion(item)) {
                    removeAnimation(itemAnimation);
                    item = -1;
                }
                break;
            case 17: // golden door
            		if (isLocked == true) {
            			if (PlayingState.getSpawners().isEmpty() && PlayingState.getEnemies().isEmpty()) {
            				isLocked = false;
            				itemAnimation.start();
            				PlayingState.setGameOver();
            			}
            		}
                else if (itemAnimation.isStopped())
                    item = -1;
                p.moveBack();
                break;
            default:
                break;
        }

        switch(sprite) { // dungeon tile
            // 0, 14, 17, 18, & 21 are tiles that should be treated as walls
            case 0:
            case 14:
            case 17:
            case 18:
            case 22:
                p.moveBack();
                break;
            case 21: // doors
            case 23:
                if (getPosition().distance(p.getPrevFootPosition()) < 12.0f) {
                    if (p.isFacingUp() == true)
                        p.setFloor(0);
                    else
                        p.setFloor(1);
                }
                break;
            case 19: // stairs
                if (getPosition().distance(p.getPrevFootPosition()) < 12.0f) {
                    if (p.isFacingUp() == true)
                        p.setFloor(2);
                    else
                        p.setFloor(1);
                }
                break;
            default:
                break;
        }
    }
    
    /** Updates tile and enemy when a enemy collides with a tile */
    public void handleEnemyCollision(Enemy e) {
        if (collides(e) == null)
            return;
        switch (item) { // item tile
            case 0: // chest
            case 1: // crates
            case 2: // door
            case 6: // trap
                e.moveBack();
                break;
            default:
                break;
        }

        switch(sprite) { // dungeon tile
            // 0, 14, 17, 18, & 21 are tiles that should be treated as walls
            case 0:
            case 14:
            case 15: // bridge
            case 16: // bridge
            case 17:
            case 18:
            case 19: // stairs
            case 21: // doors
            case 23: // doorway
                e.moveBack();
                break;
            default:
                break;
        }
    }

    /** Updates specific animations manually **/
    public void update(int delta) {
        if (isUnlockable() == true) {
            if (itemAnimation != null)
                itemAnimation.update(delta);
            
            if (bubbleAnimation != null) {
            		bubbleAnimation.update(delta);
            		
	            	if (potionAnimation.isStopped()) {
	            		if (bubbleAnimation.getFrame() == 3) {
	            			addAnimation(potionAnimation,new Vector(0,-32));
	            			potionAnimation.start();
	            		}
	            	}
	            	
	            	else {
	            		if (bubbleAnimation.getFrame() == 3) {
	            			potionAnimation.update(delta);
	            		}
	            		else {
	            			potionAnimation.stop();
	            			removeAnimation(potionAnimation);
	            		}
	            	}
            }
        }
    }

    public boolean isUnlockable() {
        switch(item) {
            case 0: // chest
            case 2: // doors
            case 17: // golden door
                return true;
            default:
                return false;
        }
    }
}
