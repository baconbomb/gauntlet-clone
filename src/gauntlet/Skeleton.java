package gauntlet;

import org.newdawn.slick.Animation;

import jig.ResourceManager;
import jig.Vector;

public class Skeleton extends Enemy {
	private static final int HEALTH = 60;
	private static final int ATTACK = 10;
	private static final int SPEED = 2;
	private static final int ATTACK_SPEED = 2;
	
	public Skeleton(Vector position, int floor) {
		super(position, HEALTH, ATTACK, ATTACK_SPEED, SPEED, floor);
		
		right_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 0, 9, 0, true, 150, true);
		right_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 1, 4, 1, true, 100, true);
		right_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 2, 9, 2, true, 50, true);
		right_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 3, 4, 3, true, 100, true);
		right_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 4, 9, 4, true, 100, true);
		
		left_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 5, 9, 5, true, 150, true);
		left_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 6, 4, 6, true, 100, true);
		left_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 7, 9, 7, true, 50, true);
		left_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 8, 4, 8, true, 100, true);
		left_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.SKELETON, 64, 64), 0, 9, 9, 9, true, 100, true);
		
		right_damage.setLooping(false);
		left_damage.setLooping(false);
		right_damage.stop();
		left_damage.stop();
		
		right_death.setLooping(false);
		left_death.setLooping(false);
		right_death.stop();
		left_death.stop();
		
        right_hurt.setLooping(false);
        left_hurt.setLooping(false);
        right_hurt.stop();
        left_hurt.stop();
		
		current = right_idle;
		addAnimation(current);
	}
}
