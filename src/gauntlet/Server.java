package gauntlet;

import java.net.*;
import java.io.*;
import java.util.LinkedList;
import java.util.Queue;

public class Server extends Thread {
    private DatagramSocket socket;
    private static final int HOST_PORT = 4445;
    private static final int CLIENT_PORT = 4446;
    private boolean running;
    private byte[] buf = new byte[2048];
    public Queue<Message> msgInQueue;
    public Queue<Message> msgOutQueue;

    public Server() {
        msgInQueue = Gauntlet.msgInQueue;
        msgOutQueue = Gauntlet.msgOutQueue;
        try {
            if (Gauntlet.isHost == true)
                socket = new DatagramSocket(HOST_PORT);
            else
                socket = new DatagramSocket(CLIENT_PORT);
            running = true;
        }
        catch(IOException i) {
            System.out.println(i);
        }
    }

    public void run() {
        try {
            while (running) {
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet);

                InetAddress address = packet.getAddress();
                int port = packet.getPort();
                //System.out.println("ip: " + address + " port: " + port);

                ByteArrayInputStream bais = new ByteArrayInputStream(buf);
                ObjectInputStream ois = new ObjectInputStream(bais);
                Message msg = (Message) ois.readObject();
                if (msg != null) {
                    if (Gauntlet.isHost == true) {
                        if (msg.getType() == MessageType.JOIN) {
                            JoinMessage jMsg = (JoinMessage) msg;
                            Client.addAddress(address);
                            //Client.addEntry(address, port);
                        }
                    }
                    //if (msg.getType() == MessageType.PLAYER) {
                    //    PlayerMessage pMsg = (PlayerMessage) msg;
                    //    //if (Gauntlet.isHost == true)
                    //    //    System.out.println("host: rcvd msg for p" + pMsg.getPlayerID() + " at " + pMsg.getPosition());
                    //    //else
                    //    //    System.out.println("client: rcvd msg for p" + pMsg.getPlayerID() + " at " + pMsg.getPosition());
                    //}
                    msgInQueue.add(msg);
                }
            }
            socket.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {
        Server server = new Server();
    }
}
