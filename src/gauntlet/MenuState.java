package gauntlet;

import java.util.LinkedList;
import java.util.Queue;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.Font;

/* 
 * Basic Menu state
 * Can be entered from other states with ESC
 */

public class MenuState extends BasicGameState {
    private Input input;
    private boolean useTextbox, waiting, failed;
    private TextField textbox;
    private Font font;
    private Queue<Message> msgInQueue;
    private Queue<Message> msgOutQueue;
    private int countdown;
    private String prev;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        input = Gauntlet.input;
        useTextbox = false;
        font = null;
        msgInQueue = Gauntlet.msgInQueue;
        msgOutQueue = Gauntlet.msgOutQueue;
        failed = false;
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        if (font == null)
            font = g.getFont();
        if (useTextbox == false) {
            g.drawString("Press 1 for Gauntlet (Host)", 20, 40);
            g.drawString("Press 2 for Gauntlet (Client)", 20, 60);
            g.drawString("Press 3 for Level Editor", 20, 80);
        }
        else {
            g.drawString("Enter the server IP address:", 20, 40);
            textbox.render(container, g);
            if (failed)
                g.drawString("Could not reach " + prev, 20, 80);
        }
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if (msgOutQueue.peek() != null)
            new Client().start();
        if (waiting == true) { // waiting to get into lobby
            if (countdown > 0) {
                loop: while (msgInQueue.peek() != null) {
                    Message msg = (Message) msgInQueue.poll();
                    switch (msg.getType()) {
                        case LOBBY:
                            msgInQueue.add(msg); // put it back on the queue to handle in lobby
                            game.enterState(Gauntlet.LOBBYSTATE);
                            input.clearKeyPressedRecord();
                            break loop;
                    }
                }
                countdown -= delta;
            }
            else {
                waiting = false;
                failed = true;
                prev = textbox.getText();
            }
        }
        else if (useTextbox == false) {
            if (input.isKeyPressed(Input.KEY_1)) {
                Gauntlet.isHost = true;
                Gauntlet.startServer();
                game.enterState(Gauntlet.LOBBYSTATE);
                input.clearKeyPressedRecord();
            }
            if (input.isKeyPressed(Input.KEY_2)) {
                Gauntlet.isHost = false;
                textbox = new TextField(container, font, 20, 60, 120, 20);
                textbox.setFocus(true);
                useTextbox = true;
            }
            if (input.isKeyPressed(Input.KEY_3)) {
                game.enterState(Gauntlet.LEVELEDITOR);
                input.clearKeyPressedRecord();
            }
        }
        else {
            if (input.isKeyPressed(Input.KEY_ENTER) || input.isKeyPressed(Input.KEY_NUMPADENTER)) {
                Client.setServerIP(textbox.getText());
                Gauntlet.startServer();
                msgOutQueue.add(new JoinMessage());
                waiting = true;
                countdown = 5000;
                failed = false;
            }
        }
    }

    @Override
    public int getID() {
        return Gauntlet.MENUSTATE;
    }

}
