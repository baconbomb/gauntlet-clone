package gauntlet;

import java.util.LinkedList;

import jig.Entity;
import jig.ResourceManager;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;

public class Spawner extends Entity {
    private static final int SIZE = 32;
    private static final int MAX_HEALTH = 300;
    private static final int MAX_DAMAGE_COOLDOWN = 500;
    private static final int SKELETON_COOLDOWN = 8000;
    private static final int SNAKE_COOLDOWN = 6000;
    private static final int GOLEM_COOLDOWN = 12000;
    private static int MAX_ENEMIES = 16;
    private final int MAX_SPAWN_COOLDOWN;
    private LinkedList<Enemy> enemies;
    private int floor;
    private int health;
    private int spawnCooldown;
    private int damageCooldown;
    private boolean dead;
    private EnemyType type;
    private Image image;

    public Spawner(Tile t, int floor) {
        super(t.getPosition());
        image = ResourceManager.getImage(Gauntlet.ITEMS).getSubImage(0, SIZE*7, SIZE, SIZE);
        addImageWithBoundingBox(image);
        switch (t.getItem()) {
            case 13:
                type = EnemyType.SKELETON;
                MAX_SPAWN_COOLDOWN = SKELETON_COOLDOWN;
                t.setTileItem(-1);
                break;
            case 14:
                type = EnemyType.SNAKE;
                MAX_SPAWN_COOLDOWN = SNAKE_COOLDOWN;
                t.setTileItem(-1);
                break;
            case 15:
                type = EnemyType.GOLEM;
                MAX_SPAWN_COOLDOWN = GOLEM_COOLDOWN;
                MAX_ENEMIES = 1;
                t.setTileItem(-1);
                break;
            default:
                type = EnemyType.SKELETON;
                MAX_SPAWN_COOLDOWN = SKELETON_COOLDOWN;
                t.setTileItem(-1);
                break;
        }
        this.floor = floor;
        health = MAX_HEALTH;
        enemies = new LinkedList<Enemy>();
        spawnCooldown = 0;
        dead = false;
    }

    public boolean removeHealth(int damage) {
        if (damageCooldown > 0) return false;
        health -= damage;
        if (health < 200 && health > 100) {
            removeImage(image);
            image = ResourceManager.getImage(Gauntlet.ITEMS).getSubImage(SIZE, SIZE*7, SIZE, SIZE);
            addImage(image);
        }
        if (health < 100 && health > 0) {
            removeImage(image);
            image = ResourceManager.getImage(Gauntlet.ITEMS).getSubImage(SIZE*2, SIZE*7, SIZE, SIZE);
            addImage(image);
        }
        if (health < 0) {
            removeImage(image);
            dead = true;
        }
        return true;
    }
    
    public int getHealth() {
    	return health;
    }

    public boolean isDead() {
        return dead;
    }

    public LinkedList<Enemy> getEnemies() {
        return enemies;
    }

    public void update(int delta, Player[] player) {
        // Creates a list of dead enemies and removes them from the dungeon
        LinkedList<Enemy> deadEnemies = new LinkedList<Enemy>();
        for (Enemy e : enemies) {
            e.update(delta, player);
            if (e.isDead() == true)
                deadEnemies.add(e);
        }
        enemies.removeAll(deadEnemies);
        for (int i = 0; i < enemies.size(); i++) {
            Enemy e1 = enemies.get(i);
            if (e1 == null) continue;
            for (int j = i + 1; j < enemies.size(); j++) {
                Enemy e2 = enemies.get(j);
                if (e2 == null) continue;
                if (e1.collides(e2) != null) {
                    e1.moveBack();
                    e2.moveBack();
                }
            }
        }

        if (damageCooldown <= 0)
            damageCooldown = MAX_DAMAGE_COOLDOWN;
        else
            damageCooldown -= delta;

        if (spawnCooldown <= 0) {
            spawnCooldown = MAX_SPAWN_COOLDOWN;
            switch (type) {
                case SKELETON:
                    enemies.add(new Skeleton(this.getPosition(), floor));
                    break;
                case SNAKE:
                    enemies.add(new Snake(this.getPosition(), floor));
                    break;
                case GOLEM:
                    enemies.add(new Golem(this.getPosition(), floor));
                    break;
            }
        }
        else
            spawnCooldown -= delta;
    }
    
    public int getFloor() {
        return floor;
    }
}
