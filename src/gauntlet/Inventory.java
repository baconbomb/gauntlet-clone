package gauntlet;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Inventory extends Entity {
	
	public static int x_offset = 45;
	public static int y_offset = 250;

	public int inventorySize = 3;
	public int numberOfItems = 0;
	private boolean isItemActive;
	private Image image;
	public Item[] items = new Item[inventorySize]; 
	
	public Inventory() {
		super(0,0);
		isItemActive = false;
		image = ResourceManager.getImage(Gauntlet.INVENTORY_BACKGROUND);
		addImage(image);
		for(int i = 0; i < inventorySize; i++) {
			items[i] = null;
		}
	}
	
	
	public boolean addItem(int item) {
		/* 8 = red
		 * 9 = yellow
		 * 10 = green
		 * 11 = blue
		 * 
		 * Stores an item entity in the array
		 * limited to inventorySize number of items
		 */
		boolean added = false;
		
		for(int i = 0; i < inventorySize; i++) {
			if(items[i] == null) {
				items[i] = new Item(item, i);
				added = true;
				numberOfItems++;
				break;
			}
		}
		return added;
	}
		
	public Item useItem(int i, Item powerUp) {
		Item item = powerUp;
		if(items[i] != null) {
			item = items[i];
			numberOfItems--;
			items[i] = null;
		}
		return item;
	}	
	
	public void emptyInventory() {
		for(int i = 0; i < inventorySize; i++) {
			items[i] = null;
		}
	}
	
	public boolean isItemActive() {
		return isItemActive;
	}
	
	public void setItemActive(boolean b) {
		isItemActive = b;
	}
	
	public void update(Vector playerPos) {
		this.setPosition(new Vector(playerPos.getX(), playerPos.getY() + Inventory.y_offset));
		for(Item i : items) {
			if(i != null) {
				i.update(playerPos);
			}
		}
	}

	public void render(Graphics g) {
		super.render(g);
		for(Item i : items) {
			if(i != null) {
				i.render(g);
			}
		}
	}
}
