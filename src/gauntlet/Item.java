package gauntlet;

import org.newdawn.slick.Animation;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class Item extends Entity{
	
	public static final int DAMAGE = 8;
	public static final int INVINCIBLE = 9;
	public static final int SPEED = 10;
	public static final int RAPID = 11;
	
	private Animation potion;
	int itemNum;
	int arrayLocation;
	
	public Item(int item, int location) {
		/* 8 = red
		 * 9 = yellow
		 * 10 = green
		 * 11 = blue
		 */
		itemNum = item;
		arrayLocation = location;
		potion = new Animation(ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32), 0, item, 7, item, true, 50, true);
		addAnimation(potion);
	}
	
	public int getItemNum() {
		return itemNum;
	}
	
	public void update(Vector playerPos) {
		this.setPosition(new Vector(playerPos.getX() - Inventory.x_offset + (arrayLocation * Inventory.x_offset), playerPos.getY() + Inventory.y_offset));
	}
}
