package gauntlet;

import org.newdawn.slick.Animation;

import jig.ResourceManager;
import jig.Vector;

public class Gladiator extends Player {
    private static final int HEALTH = 400;
    private static final int ATTACK = 40;
    private static final double DEFENSE = 0.00;
    private static final int SPEED = 2;
    private static final int ATTACK_SPEED = 3;

    public Gladiator(Vector position) {
        this(position, true);
    }

    public Gladiator(Vector position, boolean selected) {
        super(position, HEALTH, ATTACK, DEFENSE, SPEED, ATTACK_SPEED, playerType.GLADIATOR, selected);

        right_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 0, 4, 0, true, 150, true);
        right_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 1, 7, 1, true, 100, true);
        right_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 2, 6, 2, true, 50, true);
        right_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 3, 2, 3, true, 100, true);
        right_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 4, 6, 4, true, 100, true);

        left_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 5, 4, 5, true, 150, true);
        left_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 6, 7, 6, true, 100, true);
        left_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 7, 6, 7, true, 50, true);
        left_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 8, 2, 8, true, 100, true);
        left_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.GLADIATOR, 64, 64), 0, 9, 6, 9, true, 100, true);

        right_damage.setLooping(false);
        left_damage.setLooping(false);
        right_damage.stop();
        left_damage.stop();

        right_death.setLooping(false);
        left_death.setLooping(false);
        right_death.stop();
        left_death.stop();
        
        right_hurt.setLooping(false);
        left_hurt.setLooping(false);
        right_hurt.stop();
        left_hurt.stop();

        current = right_idle;
        addAnimation(current);
    }
}

