package gauntlet;

public class TileMessage extends Message {
    private int index;
    private int item;

    public TileMessage(int i, int item) {
        super(MessageType.TILE);
        this.index = i;
        this.item = item;
    }

    public int getIndex() {
        return index;
    }

    public int getItem() {
        return item;
    }
}
