package gauntlet;

import org.newdawn.slick.Image;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class HealthBar extends Entity {
	public static final int X_OFFSET = -300;
	public static final int Y_OFFSET = -280;
	Image image;
	
	public HealthBar() {
		image = ResourceManager.getImage(Gauntlet.HEALTH_10);
		addImage(image);
	}
	
	/** Updates health bar image based on player's current health **/
	public void updateHealth(int health, int max) {
		int i = (health * 10) / max;
		removeImage(image);
		
		switch(i) {
		case 0:
		case 1:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_1));
			break;
		case 2:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_2));
			break;
		case 3:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_3));
			break;
		case 4:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_4));
			break;
		case 5:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_5));
			break;
		case 6:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_6));
			break;
		case 7:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_7));
			break;
		case 8:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_8));
			break;
		case 9:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_9));
			break;
		case 10:
			addImage(ResourceManager.getImage(Gauntlet.HEALTH_10));
			break;
		}
	}
	
	/** An empty health bar is displayed when the player is dead **/
	public void empty() {
		removeImage(image);
		addImage(ResourceManager.getImage(Gauntlet.HEALTH_EMPTY));
		
	}
	
	public void update(Vector playerPos) {
		this.setPosition(new Vector(playerPos.getX() + X_OFFSET, playerPos.getY() + Y_OFFSET));
	}
	
	

}
