package gauntlet;

public class GameOverMessage extends Message {

    public GameOverMessage() {
        super(MessageType.GAMEOVER);
    }
}
