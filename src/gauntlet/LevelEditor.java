package gauntlet;

import jig.ResourceManager;
import jig.Entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


class LevelEditor extends BasicGameState {

    private static final int TILE = 32;

    private int[] cursorPos;
    private Entity cursor;
    private Map m;
    private Map map_floor0; // holds interior floor of level
    private Map map_floor1; // holds first floor of level
    private Map map_floor2; // holds second floor of level
    private boolean showBothMaps; // used to displays both floors

    private Input input;
    private Viewport viewport;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        input = Gauntlet.input;
        viewport = new Viewport(Gauntlet.WINDOW_WIDTH/2, Gauntlet.WINDOW_HEIGHT/2);
        map_floor0 = new Map(0);
        map_floor0.readMap();
        map_floor1 = new Map(1);
        map_floor1.readMap();
        map_floor2 = new Map(2);
        map_floor2.readMap();
        m = map_floor1;
        showBothMaps = false;
        cursor = new Entity(m.getCenterTile().getPosition());
        cursor.addImageWithBoundingBox(ResourceManager.getImage(Gauntlet.CURSOR));
        cursorPos = new int[] {m.getWidth()/2, m.getHeight()/2};
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) {
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        //g.drawImage(ResourceManager.getImage(Gauntlet.BG), 0, 0);
        g.translate(Gauntlet.WINDOW_WIDTH/2 - viewport.getX(), Gauntlet.WINDOW_HEIGHT/2 - viewport.getY());
        viewport.render(g);

        if (showBothMaps == true) {
            for (Tile t : map_floor1.getTiles())
                t.render(g);
            for (Tile t : map_floor2.getTiles())
                t.render(g);
        }
        else {
            for (Tile t : m.getTiles())
                if (viewport.collides(t) != null)
                    t.render(g);
            cursor.render(g);
        }
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if (input.isKeyPressed(Input.KEY_UP)) {
            cursorUp();
        }
        if (input.isKeyPressed(Input.KEY_DOWN)) {
            cursorDown();
        }
        if (input.isKeyPressed(Input.KEY_LEFT)) {
            cursorLeft();
        }
        if (input.isKeyPressed(Input.KEY_RIGHT)) {
            cursorRight();
        }
        if (input.isKeyPressed(Input.KEY_A)) {
            prevSprite();
        }
        if (input.isKeyPressed(Input.KEY_D)) {
            nextSprite();
        }
        if (input.isKeyDown(Input.KEY_S)) {
            resetSprite();
        }
        if (input.isKeyPressed(Input.KEY_ENTER)) {
            map_floor0.writeMap();
            map_floor1.writeMap();
            map_floor2.writeMap();
            //game.enterState(LevelEditorMenu.getID());
        }
        if (input.isKeyPressed(Input.KEY_0)) {
            changeMap(0);
        }
        if (input.isKeyPressed(Input.KEY_1)) {
            changeMap(1);
        }
        if (input.isKeyPressed(Input.KEY_2)) {
            changeMap(2);
        }
        if (input.isKeyDown(Input.KEY_LSHIFT) || input.isKeyDown(Input.KEY_RSHIFT)) {
            if (input.isKeyPressed(Input.KEY_R)) {
                map_floor0.addRowUp();
                map_floor1.addRowUp();
                map_floor2.addRowUp();
            }
            if (input.isKeyPressed(Input.KEY_C)) {
                map_floor0.addColLeft();
                map_floor1.addColLeft();
                map_floor2.addColLeft();
            }
            if (input.isKeyPressed(Input.KEY_I)) {
                prevItem();
            }
        }
        //else if (input.isKeyDown(Input.KEY_LCONTROL) || input.isKeyDown(Input.KEY_RCONTROL)) {
        //    if (input.isKeyPressed(Input.KEY_R)) {
        //        map_floor1.delRow(cursorPos[1]);
        //        map_floor2.delRow(cursorPos[1]);
        //    }
        //    if (input.isKeyPressed(Input.KEY_C)) {
        //        map_floor1.delCol(cursorPos[0]);
        //        map_floor2.delCol(cursorPos[0]);
        //    }
        //}
        else {
            if (input.isKeyPressed(Input.KEY_R)) {
                map_floor0.addRowDown();
                map_floor1.addRowDown();
                map_floor2.addRowDown();
            }
            if (input.isKeyPressed(Input.KEY_C)) {
                map_floor0.addColRight();
                map_floor1.addColRight();
                map_floor2.addColRight();
            }
            if (input.isKeyPressed(Input.KEY_I)) {
                nextItem();
            }
        }
        if (input.isKeyDown(Input.KEY_TAB)) {
            showBothMaps = true;
        }
        else {
            showBothMaps = false;
        }
        if(input.isKeyPressed(Input.KEY_ESCAPE)) {
            game.enterState(Gauntlet.MENUSTATE);
        }
        viewport.setPosition(cursor.getPosition()); // keep the view centered on the cursor
    }

    /** Moves cursor image up */
    private void cursorUp() {
        if (cursorPos[1] > m.getMinY()) {
            cursorPos[1]--;
            cursor.translate(0, -TILE);
        }
    }

    /** Move cursor image up */
    private void cursorDown() {
        if (cursorPos[1] < m.getMaxY()-1) {
            cursorPos[1]++;
            cursor.translate(0, TILE);
        }
    }

    /** Moves cursor image left */
    private void cursorLeft() {
        if (cursorPos[0] > m.getMinX()) {
            cursorPos[0]--;
            cursor.translate(-TILE, 0);
        }
    }

    /** Moves cursor image right */
    private void cursorRight() {
        if (cursorPos[0] < m.getMaxX()-1) {
            cursorPos[0]++;
            cursor.translate(TILE, 0);
        }
    }

    /** Changes to next sprite in dungeon sprite sheet */
    private void nextSprite() {
        for (Tile t : m.getTiles())
            if (t.collides(cursor) != null)
                t.nextSprite();
    }

    /** Changes to previous sprite in dungeon sprite sheet */
    private void prevSprite() {
        for (Tile t : m.getTiles())
            if (t.collides(cursor) != null)
                t.prevSprite();
    }

    /** Changes to first sprite in dungeon sprite sheet */
    private void resetSprite() {
        for (Tile t : m.getTiles()) {
            if (t.collides(cursor) != null) {
                t.setTileSprite(0);
                t.setTileItem(-1);
            }
        }
    }

    /** Changes to next item in item sprite sheet */
    private void nextItem() {
        for (Tile t : m.getTiles())
            if (t.collides(cursor) != null)
                t.nextItem();
    }

    /** Changes to previous item in item sprite sheet */
    private void prevItem() {
        for (Tile t : m.getTiles())
            if (t.collides(cursor) != null)
                t.prevItem();
    }

    /** Used to change floors when editing map */
    private void changeMap(int i) {
        if (i == 0) {
            m = map_floor0;
        }
        if (i == 1) {
            m = map_floor1;
        }
        if (i == 2) {
            m = map_floor2;
        }
    }

    @Override
    public int getID() {
        return Gauntlet.LEVELEDITOR;
    }
}

