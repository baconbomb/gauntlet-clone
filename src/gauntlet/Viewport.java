package gauntlet;

import jig.Entity;
import jig.ResourceManager;

public class Viewport extends Entity {
    public Viewport(int x, int y) {
        super(x,y);
        addImageWithBoundingBox(ResourceManager.getImage(Gauntlet.BG));
    }
}
