package gauntlet;

import jig.Vector;

public class EnemyMessage extends Message {
    private int index;
    private int health;
    private Vector position;

    public EnemyMessage(int i) {
        super(MessageType.ENEMY);
        index = i;
    }

    public int getIndex() {
        return index;
    }

    public Vector getPosition() {
        return position;
    }

    public int getHealth() {
        return health;
    }
}
