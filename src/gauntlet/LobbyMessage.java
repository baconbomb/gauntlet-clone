package gauntlet;

public class LobbyMessage extends Message {

    private boolean[] selected;

    public LobbyMessage(boolean[] selected) {
        super(MessageType.LOBBY);
        this.selected = selected;
    }

    public boolean[] getSelected() {
        return selected;
    }
}
