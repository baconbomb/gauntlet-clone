package gauntlet;

import org.newdawn.slick.Animation;

import jig.Entity;
import jig.ResourceManager;
import jig.Vector;

public class PotionMeter extends Entity {
	public static final int X_OFFSET = 45;
	public static final int Y_OFFSET = 285;
	private Animation current, empty, red, yellow, green, blue;
	
	
	public PotionMeter() {
		red = new Animation(ResourceManager.getSpriteSheet(Gauntlet.POTION_METER_RED, 135, 22), 0, 0, 10, 0, true, 950, true);
		yellow = new Animation(ResourceManager.getSpriteSheet(Gauntlet.POTION_METER_YELLOW, 135, 22), 0, 0, 10, 0, true, 950, true);
		green = new Animation(ResourceManager.getSpriteSheet(Gauntlet.POTION_METER_GREEN, 135, 22), 0, 0, 10, 0, true, 950, true);
		blue = new Animation(ResourceManager.getSpriteSheet(Gauntlet.POTION_METER_BLUE, 135, 22), 0, 0, 10, 0, true, 950, true);
		empty = new Animation(ResourceManager.getSpriteSheet(Gauntlet.POTION_METER, 135, 22), 0, 0, 0, 0, true, 950, true);
		
		red.setLooping(false);
		yellow.setLooping(false);
		green.setLooping(false);
		blue.setLooping(false);
		
		current = empty;
		addAnimation(current);
	}
	
	/** Updates potion meter when a potion is being used **/
	public void updateMeter(int i) {
		removeAnimation(current);
		
		switch(i) {
			case 8:
				current = red;
				break;
			case 9:
				current = yellow;
				break;
			case 10:
				current = green;
				break;
			case 11:
				current = blue;
				break;
			case 12:
				current = empty;
				break;
		}
		addAnimation(current);
		current.restart();
	}
	
	public void update(Vector playerPos) {
		this.setPosition(new Vector(playerPos.getX(), playerPos.getY() + PotionMeter.Y_OFFSET));
	}

}
