package gauntlet;

import java.util.LinkedList;
import java.util.Queue;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.ResourceManager;
import jig.Vector;

public class LobbyState extends BasicGameState {
	Input input;
	private int cursor; // used for selecting and confirming options made by the player
	private boolean ready; // used to confirm if player is ready to start game
	private Adventurer adventurer;
	private Warrior warrior;
	private Wizard wizard;
	private Gladiator gladiator;
    private Queue<Message> msgInQueue;
    private Queue<Message> msgOutQueue;
    private boolean[] selected;
    private boolean touched;

	
	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		input = container.getInput();
		adventurer = new Adventurer(new Vector(295,252));
		warrior = new Warrior(new Vector(358,252));
		wizard = new Wizard(new Vector(430,250));
		gladiator = new Gladiator(new Vector(500,252));	
        msgInQueue = Gauntlet.msgInQueue;
        msgOutQueue = Gauntlet.msgOutQueue;
        selected = new boolean[4];
        touched = false;
	}
	
	
	public void enter(GameContainer container, StateBasedGame game) {
        cursor = 0;
        handleMsgQueue(game);
        if (selected[cursor] == true)
            right();
        ready = false;
	}

	
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.drawImage(ResourceManager.getImage(Gauntlet.BG2), 0, 0); // background
		g.drawImage(ResourceManager.getImage(Gauntlet.LOBBY_BANNER), 230, 120); // banner
		updateImages(g);	
		adventurer.render(g);
		warrior.render(g);
		wizard.render(g);
		gladiator.render(g);
		isReady(g);
	}
	
	
	/** Updates images based on player's cursor value **/
	public void updateImages(Graphics g) {
		if (cursor == 0) { // Adventurer
			g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_IMAGE_1), 260, 225);
			g.drawImage(ResourceManager.getImage(Gauntlet.ADVENTURER_STATS), 269, 300);
			adventurer.updateAnimation(adventurer.right_walk, null);
			
			gladiator.updateAnimation(gladiator.right_idle, null);
			warrior.updateAnimation(warrior.right_idle, null);
		}
		
		else if (cursor == 1) { // Warrior
			g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_IMAGE_2), 260, 225);
			g.drawImage(ResourceManager.getImage(Gauntlet.WARRIOR_STATS), 269, 300);
			warrior.updateAnimation(warrior.right_walk, null);
			
			adventurer.updateAnimation(adventurer.right_idle, null);
			wizard.updateAnimation(wizard.right_idle, null);
		}
		
		else if (cursor == 2) { // Wizard
			g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_IMAGE_3), 260, 225);
			g.drawImage(ResourceManager.getImage(Gauntlet.WIZARD_STATS), 269, 300);
			wizard.updateAnimation(wizard.right_walk, null);
			
			warrior.updateAnimation(warrior.right_idle, null);
			gladiator.updateAnimation(gladiator.right_idle, null);	
		}
		
		else if (cursor == 3) { // Gladiator
			g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_IMAGE_4), 260, 225);
			g.drawImage(ResourceManager.getImage(Gauntlet.GLADIATOR_STATS), 269, 300);
			gladiator.updateAnimation(gladiator.right_walk, null);
			
			adventurer.updateAnimation(adventurer.right_idle, null);
			wizard.updateAnimation(wizard.right_idle, null);	
		}			
	}
	
	
	/** Updates cursor and sets player as character selected **/
	public void isReady(Graphics g) {
        if (selected[0] == true) { // Adventurer
            g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_READY), 260, 211);
        }

        if (selected[1] == true) { // Warrior
            g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_READY), 329, 211);
        }

        if (selected[2] == true) { // Wizard
            g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_READY), 398, 211);
        }

        if (selected[3] == true) { // Gladiator
            g.drawImage(ResourceManager.getImage(Gauntlet.CURSOR_READY), 467, 211);
        }

		if (ready == true) {
			//enter button
            if (Gauntlet.isHost == true)
                g.drawImage(ResourceManager.getImage(Gauntlet.ENTER_BUTTON), 300, 450);
            else
                g.drawImage(ResourceManager.getImage(Gauntlet.WAITING_BUTTON), 305, 450);
		}
	}
	
	/** Updates cursor value when the left arrow key is pressed **/
	public void left() {
		cursor = (cursor + 3) % 4;
        if (selected[cursor] == true) // if the player is selected, then move onto the next
            left();
	}
	
	
	/** Updates cursor value when the right arrow key is pressed **/
	public void right() {
		cursor = (cursor + 1) % 4;
        if (selected[cursor] == true) // if the player is selected, then move onto the next
            right();
	}
	
	/** Ready is true when player has selected a character **/
	public void confirm(StateBasedGame game) {
        PlayingState.setPlayerID(cursor);
        ready = true;
        selected[cursor] = true;
        touched = true;
	}

	
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		// character has not been selected
		if (ready == false) {
            if (selected[cursor]) right();
			if (input.isKeyPressed(Input.KEY_LEFT))
				left();
			if (input.isKeyPressed(Input.KEY_RIGHT))
				right();
			if (input.isKeyPressed(Input.KEY_ENTER))
				confirm(game);
		}
		
        // send updates if anything happens
        if (touched == true) {
            msgOutQueue.add(new LobbyMessage(selected));
            touched = false;
        }
        handleMsgQueue(game);
        if (msgOutQueue.peek() != null)
            new Client().start();

		// character has been selected
		if (ready == true) {
            if (Gauntlet.isHost == true) {
                if (input.isKeyPressed(Input.KEY_ENTER)) {
                    msgOutQueue.add(new JoinMessage());
                    PlayingState.addPlayers(selected);
                    game.enterState(Gauntlet.PLAYINGSTATE);
                }
            }
		}
	}

    private void handleMsgQueue(StateBasedGame game) {
        while (msgInQueue.peek() != null) {
            Message msg = (Message) msgInQueue.poll();
            switch (msg.getType()) {
                case JOIN:
                    if (Gauntlet.isHost == false && ready == true) {
                        PlayingState.addPlayers(selected);
                        game.enterState(Gauntlet.PLAYINGSTATE);
                    }
                    else if (Gauntlet.isHost == true)
                        msgOutQueue.add(new LobbyMessage(selected));
                    break;
                case LOBBY:
                    LobbyMessage lobbyMsg = (LobbyMessage) msg;
                    boolean[] sel = lobbyMsg.getSelected();
                    for (int i = 0; i < selected.length; i++)
                        selected[i] = selected[i] || sel[i];
                    if (Gauntlet.isHost == true)
                        touched = true;
                    break;
            }
        }
    }

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return Gauntlet.LOBBYSTATE;
	}

}
