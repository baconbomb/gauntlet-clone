package gauntlet;

public class JoinMessage extends Message {

    public JoinMessage() {
        super(MessageType.JOIN);
    }
}
