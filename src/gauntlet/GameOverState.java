package gauntlet;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.ResourceManager;
import jig.Vector;

public class GameOverState extends BasicGameState{
	Input input;
	static int[] score;
	private Image scoreImage;
	private Adventurer adventurer;
	private Warrior warrior;
	private Wizard wizard;
	private Gladiator gladiator;
	
	
	public static void updateScore(int playerID, int s) {
		score[playerID] = s;
	}

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		input = container.getInput();
		adventurer = new Adventurer(new Vector(100,252));
		warrior = new Warrior(new Vector(300,252));
		wizard = new Wizard(new Vector(500,250));
		gladiator = new Gladiator(new Vector(700,252));	
		score = new int[4];
		scoreImage = ResourceManager.getImage(Gauntlet.COIN);
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
		g.drawImage(ResourceManager.getImage(Gauntlet.BG2), 0, 0); // background
		g.drawImage(ResourceManager.getImage(Gauntlet.GAMEOVER_BANNER), 280, 120); // banner
		g.drawImage(ResourceManager.getImage(Gauntlet.MENU_BUTTON), 280, 450); // menu button
		adventurer.render(g);
		warrior.render(g);
		wizard.render(g);
		gladiator.render(g);
		
		g.drawImage(scoreImage, 60, 300);
		g.drawImage(scoreImage, 260, 300);
		g.drawImage(scoreImage, 460, 300);
		g.drawImage(scoreImage, 660, 300);
		g.drawString("" + score[0], 85, 302); 
		g.drawString("" + score[1], 285, 302);
		g.drawString("" + score[2], 485, 302);
		g.drawString("" + score[3], 685, 302);

	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
		if (input.isKeyPressed(Input.KEY_ENTER)) {
			game.enterState(Gauntlet.TITLESTATE);
		}
	}

	@Override
	public int getID() {
		return Gauntlet.GAMEOVERSTATE;
	}

}
