package gauntlet;

import jig.Vector;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class Map {

    private static final int TILE_OFFSET = 16;

    private String filename;
    private int minX;
    private int maxX;
    private int minY;
    private int maxY;

    private ArrayList<Tile> tiles;

    public Map(int i) {
        tiles = new ArrayList<Tile>();
        minX = 0;
        minY = 0;

        if (i == 0) {
            filename = Gauntlet.MAP1_ROOMS;
        }
        else if (i == 1) {
            filename = Gauntlet.MAP1_LOWER;
        }
        else if (i == 2) {
            filename = Gauntlet.MAP1_UPPER;
        }
    }

    /** Returns a small subset of 9 tiles around the given position */
    public ArrayList<Tile> nearTiles(Vector position) {
        if (position == null)
            return null;
        int x = (int) position.getX()/32;
        int y = (int) position.getY()/32;
        int t = x + y*getWidth();
        if (t < 0)
            return null;
        ArrayList<Tile> subset = new ArrayList<Tile>();
        subset.add(tiles.get(t));
        int l = getLeft(t);
        if (l != -1) {
            subset.add(tiles.get(l));
            int u = getUp(l);
            if (u != -1)
                subset.add(tiles.get(u));
            int d = getDown(l);
            if (d != -1)
                subset.add(tiles.get(d));
        }
        int r = getRight(t);
        if (r != -1) {
            subset.add(tiles.get(r));
            int u = getUp(r);
            if (u != -1)
                subset.add(tiles.get(u));
            int d = getDown(r);
            if (d != -1)
                subset.add(tiles.get(d));
        }
        int u = getUp(t);
        if (u != -1)
            subset.add(tiles.get(u));
        int d = getDown(t);
        if (d != -1)
            subset.add(tiles.get(d));
        return subset;
    }

    public int getUp(int i) {
        int index = i - getWidth();
        if (index < 0) return -1;
        else return index;
    }

    public int getDown(int i) {
        int index = i + getWidth();
        if (index >= tiles.size()) return -1;
        else return index;
    }

    public int getLeft(int i) {
        int index = i - 1; // TODO: correct wrap around
        if (index < 0) return -1;
        else return index;
    }

    public int getRight(int i) {
        int index = i + 1;
        if (index >= tiles.size()) return -1;
        else return index;
    }

    /** Used to read the map file and draw tiles */
    public void readMap() {
        FileInputStream fis = null;
        DataInputStream dis = null;

        try {
            File f = new File(filename);
            fis = new FileInputStream(f);
            dis = new DataInputStream(fis);

            //for (int i = 0; i < 19; i++) {
            //    for (int j = 0; j < 25; j++) {
            //        char c = dis.readChar();
            //        Tile t = new Tile(TILE_OFFSET+32*j, TILE_OFFSET+32*i, 0, c);
            //        tiles.add(t);
            //    }
            //}
            //maxX = 25;
            //maxY = 19;

            int i = 0;
            int j = 0;
            while (dis.available() > 0) {
                char c = dis.readChar();
                if (c == '\n') {
                    if (i == 0)
                        maxX = j;
                    i++;
                    j = 0;
                    continue;
                }
                else {
                    Tile t = new Tile(TILE_OFFSET+32*j, TILE_OFFSET+32*i, c);
                    tiles.add(t);
                    j++;
                }
            }
            maxY = i;

        }
        catch (FileNotFoundException fnfe) {
            System.out.println("File not found" + fnfe);
        }
        catch (IOException ioe) {
            System.out.println("Error while writing to file" + ioe);
        }
        finally {
            try {
                if (dis != null) {
                    dis.close();
                }
                if (fis != null) {
                    fis.close();
                }
            }
            catch (Exception e) {
                System.out.println("Error while closing streams" + e);
            }
        }
    }

    /** Used to create a map
     * Writes characters associated with a tile to a file
     * */
    public void writeMap() {
        FileOutputStream fos = null;
        DataOutputStream dos = null;

        try {
            File f = new File(filename);
            fos = new FileOutputStream(f);
            dos = new DataOutputStream(fos);

            for (int i = 0; i < getHeight(); i++) {
                for (int j = 0; j < getWidth(); j++) {
                    Tile t = tiles.get(j + i*getWidth());
                    char c = t.toChar();
                    dos.writeChar(c); // writes the character to the file
                }
                dos.writeChar('\n'); // end each row with a newline
            }

        }
        catch (FileNotFoundException fnfe) {
            System.out.println("File not found" + fnfe);
        }
        catch (IOException ioe) {
            System.out.println("Error while writing to file" + ioe);
        }
        finally {
            try {
                if (dos != null) {
                    dos.close();
                }
                if (fos != null) {
                    fos.close();
                }
            }
            catch (Exception e) {
                System.out.println("Error while closing streams" + e);
            }
        }
    }

    /** Adds a row of tiles to the top */
    public void addRowUp() {
        minY--;
        for (int j = 0; j < getWidth(); j++) {
            Tile t = new Tile(TILE_OFFSET+32*j, TILE_OFFSET+32*minY, 0, 0);
            tiles.add(j, t);
        }
    }

    /** Adds a row of tiles to the bottom */
    public void addRowDown() {
        for (int j = 0; j < getWidth(); j++) {
            Tile t = new Tile(TILE_OFFSET+32*j, TILE_OFFSET+32*maxY, 0, 0);
            tiles.add(t);
        }
        maxY++;
    }

    ///** Removes a row */
    //public void delRow(int y) {
    //    for (int j = 0; j < getWidth(); j++) {
    //        Tile t = new Tile(TILE_OFFSET+32*j, TILE_OFFSET+32*y, 0, 0);
    //        tiles.remove(t);
    //    }
    //    maxY--;
    //}

    /** Adds a end of tiles to the left */
    public void addColLeft() {
        minX--;
        for (int i = 0; i < getHeight(); i++) {
            Tile t = new Tile(TILE_OFFSET+32*minX, TILE_OFFSET+32*i, 0, 0);
            tiles.add(i*getWidth(), t);
        }
    }

    /** Adds a end of tiles to the right */
    public void addColRight() {
        maxX++;
        for (int i = 0; i < getHeight(); i++) {
            Tile t = new Tile(TILE_OFFSET+32*(maxX-1), TILE_OFFSET+32*i, 0, 0);
            tiles.add((i+1)*getWidth()-1, t);
        }
    }

    ///** Removes a column */
    //public void delCol(int x) {
    //    for (int i = 0; i < getHeight(); i++) {
    //        tiles.remove(i);
    //    }
    //    maxX--;
    //}

    /** Returns the map width in tiles */
    public int getWidth() {
        return maxX - minX;
    }

    /** Returns the map height in tiles */
    public int getHeight() {
        return maxY - minY;
    }

    /** Returns the min x value */
    public int getMinX() {
        return minX;
    }

    /** Returns the max x value */
    public int getMaxX() {
        return maxX;
    }

    /** Returns the min y value */
    public int getMinY() {
        return minY;
    }

    /** Returns the max y value */
    public int getMaxY() {
        return maxY;
    }

    /** Returns the tile at (x, y) */
    public Tile getTile(int x, int y) {
        return tiles.get(y*getWidth() + x);
    }

    /** Returns the center tile */
    public Tile getCenterTile() {
        return getTile(getWidth()/2, getHeight()/2);
    }

    /** Returns the dungeon map */
    public ArrayList<Tile> getTiles() {
        return tiles;
    }

}
