package gauntlet;

import java.awt.Font;
import java.util.Queue;

import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.ResourceManager;

public class TitleState extends BasicGameState {
	Input input;
	private int cursor;
    private boolean useTextbox, waiting, failed;
    private TextField textbox;
    private TrueTypeFont font;
    private Queue<Message> msgInQueue;
    private Queue<Message> msgOutQueue;
    private int countdown;

	@Override
	public void init(GameContainer container, StateBasedGame game) throws SlickException {
		// TODO Auto-generated method stub
		input = container.getInput();
        useTextbox = false;
        Font f = new Font("Serif", Font.BOLD, 24);
        font = new TrueTypeFont(f, false);
        msgInQueue = Gauntlet.msgInQueue;
        msgOutQueue = Gauntlet.msgOutQueue;
        failed = false;
		
	}
	
	public void enter(GameContainer container, StateBasedGame game) {
        cursor = 0;
	}

	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        g.drawImage(ResourceManager.getImage(Gauntlet.BG2), 0, 0); // background
        //if (font == null) {
        //    font = g.getFont();
        //    font = font.deriveFont(24.0f);
        //}
        if (useTextbox == true) {
            g.drawImage(ResourceManager.getImage(Gauntlet.MESSAGE_BOX), 250, 200);
            g.drawImage(ResourceManager.getImage(Gauntlet.IP_BANNER), 270, 230);
            g.drawImage(ResourceManager.getImage(Gauntlet.IP_INPUT), 294, 274);
            textbox.render(container, g);
            //if (failed)
                g.drawImage(ResourceManager.getImage(Gauntlet.FAILED_BANNER), 311, 344);
        }
        else {
            g.drawImage(ResourceManager.getImage(Gauntlet.TITLE_BANNER), 240, 120); // banner
            g.drawImage(ResourceManager.getImage(Gauntlet.HOST_BUTTON_C), 280, 280); 
            g.drawImage(ResourceManager.getImage(Gauntlet.CLIENT_BUTTON), 280, 380);

            updateImages(g);
        }
		
	}
	
	/** Updates images based on player's cursor value **/
	public void updateImages(Graphics g) {
		if (cursor == 0) { // host
			g.drawImage(ResourceManager.getImage(Gauntlet.HOST_BUTTON_C), 280, 280);
			g.drawImage(ResourceManager.getImage(Gauntlet.CLIENT_BUTTON), 280, 380);
		}
		
		else if (cursor == 1) { // client
			g.drawImage(ResourceManager.getImage(Gauntlet.CLIENT_BUTTON_C), 280, 380);
			g.drawImage(ResourceManager.getImage(Gauntlet.HOST_BUTTON), 280, 280);
		}
	}
	
	/** Updates cursor value when the up arrow key is pressed **/
	public void up() {
		cursor = (cursor + 1) % 2;
	}
	
	
	/** Updates cursor value when the down arrow key is pressed **/
	public void down() {
		cursor = (cursor + 1) % 2;
	}

	@Override
	public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if (msgOutQueue.peek() != null)
            new Client().start();
        if (waiting == true) { // waiting to get into lobby
            if (countdown > 0) {
                loop: while (msgInQueue.peek() != null) {
                    Message msg = (Message) msgInQueue.poll();
                    switch (msg.getType()) {
                        case LOBBY:
                            msgInQueue.add(msg); // put it back on the queue to handle in lobby
                            game.enterState(Gauntlet.LOBBYSTATE);
                            input.clearKeyPressedRecord();
                            break loop;
                    }
                }
                countdown -= delta;
            }
            else {
                waiting = false;
                failed = true;
            }
        }
        else if (useTextbox == false) {
            if (input.isKeyPressed(Input.KEY_ENTER)) {
                if (cursor == 0) {
                    Gauntlet.isHost = true;
                    Gauntlet.startServer();
                    game.enterState(Gauntlet.LOBBYSTATE);
                    input.clearKeyPressedRecord();
                }
                else {
                    Gauntlet.isHost = false;
                    textbox = new TextField(container, font, 320, 280, 160, 40);
                    textbox.setFocus(true);
                    useTextbox = true;
                }
            }
            if (input.isKeyPressed(Input.KEY_UP)) {
                up();
            }
            if (input.isKeyPressed(Input.KEY_DOWN)) {
                down();
            }
        }
        else {
            if (input.isKeyPressed(Input.KEY_ESCAPE)) {
                useTextbox = false;
            }
            if (input.isKeyPressed(Input.KEY_ENTER) || input.isKeyPressed(Input.KEY_NUMPADENTER)) {
                Client.setServerIP(textbox.getText());
                Gauntlet.startServer();
                msgOutQueue.add(new JoinMessage());
                waiting = true;
                countdown = 5000;
                failed = false;
            }
        }

	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return Gauntlet.TITLESTATE;
	}

}
