package gauntlet;

import org.newdawn.slick.Animation;

import jig.ConvexPolygon;
import jig.Entity;
import jig.Vector;

import org.newdawn.slick.Graphics;

public abstract class Player extends Entity {
	
	public enum playerType {
		ADVENTURER, WARRIOR, WIZARD, GLADIATOR; 
	}
	
	private final playerType type;
	
    private int health, attack, keys, floor, damageCooldown, maxHealth;
    private double defense;
    private float speed, attack_speed;
    private boolean right, idle, up, dead, touched, selected;
    private Vector prevPosition, respawnPosition;
    private ConvexPolygon weaponHitbox;

    public Item powerUp;
    public int buffTimer, deathTimer, attackTimer;

    public Inventory inventory;
    public HealthBar healthbar;
    public PotionMeter meter;
    private int score;

    public Animation right_idle, right_walk, right_damage, right_hurt, right_death;
    public Animation current, left_idle, left_walk, left_damage, left_hurt, left_death;

    public Player(Vector position, int health, int attack, double defense, int speed, int attack_speed, playerType type, boolean selected) {
        super(position);
        prevPosition = new Vector(position);
        respawnPosition = new Vector(position);
        this.health = health;
        this.maxHealth = health;
        this.attack = attack;
        this.defense = defense;
        this.speed = speed;
        this.attack_speed = attack_speed;
        this.type = type;
        this.selected = selected;

        inventory = new Inventory();
        healthbar = new HealthBar();
        meter = new PotionMeter();

        keys = 0;
        floor = 2;
        damageCooldown = 0;
        right = true;
        idle = true;
        dead = false;
        touched = false;

        if (selected)
            addShape(new ConvexPolygon(8,8),new Vector (0,25)); // hit box near feet
        else
            translate(-2000, -2000);
        weaponHitbox = new ConvexPolygon(16,32);
    }
    
    @Override
    public void render(Graphics g) {
        if (selected == true)
            super.render(g);
    }

    public void touch() {
        touched = true;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setScore(int i) {
    	score = i;
    }
    
    public int getScore() {
    	return score;
    }
    
    public void addPlayerScore(int i) {
    	score += i;
    }
    
    public void reducePlayerScore() {
    	score = score/2;
    }

    /** Returns player's previous position **/
    public Vector getPrevPosition() {
        return prevPosition;
    }

    /** Returns previous position of player's feet **/
    public Vector getPrevFootPosition() {
        Vector v = new Vector(0, 25);
        return getPrevPosition().copy().add(v);
    }

    /** Returns position of player's feet **/
    public Vector getFootPosition() {
        Vector v = new Vector(0, 25);
        return getPosition().copy().add(v);
    }

    /** Moves player back when collision occurs **/
    public void moveBack() {
        setPosition(getPrevPosition());
    }

    /** Returns player's current health **/
    public int getHealth() {
        return health;
    }

    /**Returns characters max health**/
    public int getMaxHealth() {
        return maxHealth;
    }

    /** Update players health **/
    public void setHealth(int health) {
        this.health = health;
        healthbar.updateHealth(health, maxHealth);
        touch();
    }

    /** Decrements health by a variable amount **/
    public void removeHealth(int damage) {
        if(!hasInvincible()) {
        	if (damageCooldown <= 0) {
	        
	            int h = getHealth();
	            h -= damage;
	            setHealth(h);
	
	            if (h <= 0) { // player is dead
	                healthbar.empty();
	                death();
	            }
	            else {
	                damageCooldown = 1000;
	                hurt();
	            }
	        }
        }
    }

    /** Increments health by a variable amount **/
    public void addHealth(int health) {
        int h = getHealth();
        h += health;
        setHealth(h);
    }

    /** Causes damage to enemies and objects **/
    public void attack() {

        if (attackTimer <= 0) {
            int x;
            if (right == true)
                x = 16;
            else
                x = -16;

            addShape(weaponHitbox, new Vector(x,8));
            idle = false;
            attackTimer = (int) (3000 / (getAttackSpeed()*1.2f));
            updateAnimation(right_damage, left_damage);
            touch();
        }
    }

    /** Returns whether player is facing right **/
    public boolean isRight() {
        return right;
    }

    /** Sets whether player is facing right **/
    public void setRight(boolean r) {
        right = r;
    }

    /** Returns whether player is facing up **/
    public boolean isUp() {
        return up;
    }

    /** Sets whether player is facing up **/
    public void setUp(boolean u) {
        up = u;
    }

    /** Returns player's attack value **/
    public int getAttack() {
        if(hasDamageBuff()) {		//player has damage buff
        	return attack * 2;		//double damage
        }
    	return attack;
    }

    /** Returns player's attack speed **/
    public float getAttackSpeed() {
        return hasRapidFire(attack_speed);
    }

    /** Checks if attack animation is currently playing **/
    public boolean isAttacking() {
        if (right_damage.isStopped() == false || left_damage.isStopped() == false) {
            return true;
        }
        return false;
    }

    /** Returns player's defense value **/
    public double getDefense() {
        return defense;
    }

    /** Moves player left **/
    public void moveLeft(int delta) {
        right = false;
        idle = false;
        updateAnimation(right_walk, left_walk);
        this.setX(this.getX() - hasSpeedBuff(speed));
        touch();
    }

    /** Moves player right **/
    public void moveRight(int delta) {
        right = true;
        idle = false;
        updateAnimation(right_walk, left_walk);
        this.setX(this.getX() + hasSpeedBuff(speed));
        touch();
    }

    /** Moves player up **/
    public void moveUp(int delta) {
        idle = false;
        up = true;
        updateAnimation(right_walk, left_walk);
        this.setY(this.getY() - hasSpeedBuff(speed));
        touch();
    }

    /** Moves player down **/
    public void moveDown(int delta) {
        idle = false;
        up = false;
        updateAnimation(right_walk, left_walk);
        this.setY(this.getY() + hasSpeedBuff(speed));
        touch();
    }

    /** Displays idle animation **/
    public void idle() {
        updateAnimation(right_idle, left_idle);
    }

    /** Sets idle flag **/
    public void setIdle(boolean idle) {
        this.idle = idle;
    }

    /** Displays hurt animation **/
    public void hurt() {
        setIdle(false);
        updateAnimation(right_hurt, left_hurt);
    }
    
    /** Checks if hurt animation is currently playing **/
    public boolean isHurt() {
        if (right_hurt.isStopped() == false || left_hurt.isStopped() == false) {
            return true;
        }
        return false;
    }

    /** Displays death animation **/
    public void death() {
        setIdle(false);
        dead = true;
        updateAnimation(right_death, left_death);
        touch();
        deathTimer = 3200;
        damageCooldown = 4000;
    }

    /** Checks if death animation is currently playing **/
    public boolean isDead() {
        if (right_death.isStopped() == false || left_death.isStopped() == false) {
            return true;
        }
        return false;
    }

    /** Resets player's stats and location after death **/
    public void respawn(Player[] player) {
    		for (int i = 0; i < player.length; i++)
    			if (this == player[i]) {
    				switch(type) {
    					case ADVENTURER:
    						player[i] = new Adventurer(respawnPosition, true);
    						break;
    					case WARRIOR:
    						player[i] = new Warrior(respawnPosition, true);
    						break;
    					case GLADIATOR:
    						player[i] = new Gladiator(respawnPosition, true);
    						break;
    					case WIZARD:
    						player[i] = new Wizard(respawnPosition, true);
    						break;
    				}
    			}
    }
    
    public boolean isWizard(Player p) {
    	if(p.type == playerType.WIZARD) {
    		return true;
    	}
    	else return false;
    }

    /** Checks if player is moving up or down **/
    public boolean isFacingUp() {
        return up;
    }

    /** Updates player's map (floor 0, 1 or 2) **/
    public void setFloor(int f) {
        floor = f;
        touch();
    }

    /** Get the current floor that the player is on **/
    public int getFloor() {
        return floor;
    }

    /** Returns the player's current key amount **/
    public int getKeyCount() {
        return keys;
    }

    /** Set the number of keys a player has **/
    public void setKeyCount(int i) {
        keys = i;
        touch();
    }
    
    /** Remove i number of keys from a player  **/
    public void removeKey(int i) {
        keys -= i;
        touch();
    }

    /** Updates player's key amount **/
    public void addtKeyCount(int i) {
        keys += i;
        touch();
    }

    /** Add a potion to player inventory
     * @param itemNum
     **/
    public boolean addPotion(int i) {
        return inventory.addItem(i);
    }

    /** Updates player's animation **/
    public void updateAnimation(Animation r, Animation l) {
        if (isAttacking() == false && isDead() == false && isHurt() == false) {
            removeAnimation(current);
            current = right ? r : l;
            addAnimation(current);
        }

        if (dead == true)
            current.setDuration(current.getFrameCount()-1, 2500); // stop death animation on last frame

        if (current.isStopped() == true)
            current.restart();
    }
    
    /**  player uses an item in inventory slot i**/
    public void useItem(int i) {
        if (inventory.isItemActive() == false) {
            powerUp = inventory.useItem(i, powerUp);

            if (powerUp != null) {
                inventory.setItemActive(true);
                buffTimer = 10000;
                meter.updateMeter(powerUp.getItemNum());
            }
        }
    }
    /** player has damage buff **/
    private boolean hasDamageBuff() {
        if(powerUp != null) {
            if(powerUp.getItemNum() == Item.DAMAGE) {
                return true;
            }
        }
        return false;
    }

    /** player has invincible buff  **/
    private boolean hasInvincible() {
        if(powerUp != null) {
            if(powerUp.getItemNum() == Item.INVINCIBLE) {
                return true;
            }
        }
        return false;
    }

    /** player has speed buff **/
    private float hasSpeedBuff(float speed) {
        float powerUpSpeed = speed;
        if(powerUp != null) {
            if(powerUp.getItemNum() == Item.SPEED) {
                return powerUpSpeed * 1.5f;
            }
        }
        return powerUpSpeed;
    }

    /** player has rapid fire buff **/
    private float hasRapidFire(float attack_speed) {
        float powerUpSpeed = attack_speed;
        if(powerUp != null) {
            if(powerUp.getItemNum() == Item.RAPID) {
                right_damage.setSpeed(2);
                left_damage.setSpeed(2);
                return powerUpSpeed * 3f;
            }
        }
        right_damage.setSpeed(1);
        left_damage.setSpeed(1);
        return powerUpSpeed;
    }

    public void update(int delta, Player[] player) {
        if (!selected) return;
        touched = false;
        if(buffTimer > 0) {
            buffTimer -= delta;
        }
        else {
            powerUp = null;
            meter.updateMeter(12);
            inventory.setItemActive(false);
        }

        if (right_damage.isStopped() == true && left_damage.isStopped() == true) {
            if (right_death.isStopped() == true && left_death.isStopped() == true) {
                removeShape(weaponHitbox);
                if (idle == true)
                    idle();
                else
                    idle = true;
            }
        }

        if (damageCooldown > 0)
            damageCooldown -= delta;
        if (attackTimer > 0)
            attackTimer -= delta;

        if (deathTimer > 0)
            deathTimer -= delta;
        else {
            if (dead == true)
                respawn(player);
        }

        prevPosition = getPosition();
    }

}
