package gauntlet;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import jig.ResourceManager;
import jig.Vector;


class PlayingState extends BasicGameState {
	
	boolean debugON = false;		//flag for print outs on screen for debugging
	
    private Input input;
    private Viewport viewport;
    private static int playerID;
    private static Player[] player;
    private static LinkedList<Enemy> enemies;
    private static LinkedList<Spawner> spawners;
    private LinkedList<Tile> unlockables;
    private boolean showBothMaps;

    private Map m;
    private Map[] map_floor;
    private static Queue<Message> msgInQueue;
    private static Queue<Message> msgOutQueue;
    private static Queue<Projectile> projectiles;
    private Image keyImage, coinImage;
    
    private static final int spawnerScore = 200;
    private static final int enemyScore = 100;
    private static final int wizardNum = 2;

    public static void addProjectile(Projectile proj) {
        projectiles.add(proj);
    }

    public static void addPlayers(boolean[] selected) {
        player[0] = new Adventurer(new Vector(500,610), selected[0]);
        player[1] = new Warrior(new Vector(540,610), selected[1]);
        player[2] = new Wizard(new Vector(580,610), selected[2]);
        player[3] = new Gladiator(new Vector(625,610), selected[3]);
    }

    public static void setPlayerID(int id) {
        playerID = id;
    }

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        input = Gauntlet.input;
        msgInQueue = Gauntlet.msgInQueue;
        msgOutQueue = Gauntlet.msgOutQueue;
        projectiles = new LinkedList<Projectile>();
		keyImage = ResourceManager.getSpriteSheet(Gauntlet.ITEMS, 32, 32).getSubImage(0, 5);
		coinImage = ResourceManager.getImage(Gauntlet.COIN);
        viewport = new Viewport(Gauntlet.WINDOW_WIDTH/2, Gauntlet.WINDOW_HEIGHT/2);
        map_floor = new Map[3];
        map_floor[0] = new Map(0);
        map_floor[0].readMap();
        map_floor[1] = new Map(1);
        map_floor[1].readMap();
        map_floor[2] = new Map(2);
        map_floor[2].readMap();
        m = map_floor[2];
        showBothMaps = true;
        player = new Player[4];
        //player[0] = new Adventurer(new Vector(500,610));
        //player[1] = new Warrior(new Vector(540,610));
        //player[2] = new Wizard(new Vector(580,610));
        //player[3] = new Gladiator(new Vector(625,610));

        enemies = new LinkedList<Enemy>();
        spawners = new LinkedList<Spawner>();
        unlockables = new LinkedList<Tile>();
        for (int i = 0; i < map_floor.length; i++)
            for (Tile t : map_floor[i].getTiles()) {
                if (t.getSprite() == 22)
                    spawners.add(new Spawner(t, i));
                if (t.isUnlockable() == true)
                    unlockables.add(t);
            }
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) {
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        g.translate(Gauntlet.WINDOW_WIDTH/2 - viewport.getX(), Gauntlet.WINDOW_HEIGHT/2 - viewport.getY());
        viewport.render(g);

        showBothMaps = (m == map_floor[0]) ? false : true;
        if (showBothMaps == true) {
            for (Tile t : map_floor[1].getTiles())
                if (viewport.collides(t) != null)
                    t.render(g);
            for (Tile t : map_floor[2].getTiles())
                if (viewport.collides(t) != null)
                    t.render(g);
        }
        else {
            for (Tile t : m.getTiles())
                if (viewport.collides(t) != null)
                    t.render(g);
        }

        for (Spawner s : spawners) {
            if (!showBothMaps) { //floor 0
                if (s.getFloor() == 0) {
                    s.render(g);
                    for (Enemy e : s.getEnemies())
                        e.render(g);
                }
            }
            else { //not in floor 0
                if (s.getFloor() != 0) {
                    s.render(g);
                    for (Enemy e : s.getEnemies())
                        e.render(g);
                }
            }
        }
        for (Enemy e : enemies) {
            if (!showBothMaps) { //floor 0
                if (e.getFloor() == 0)
                    e.render(g);
            }
            else { //not floor 0
                if (e.getFloor() != 0)
                    e.render(g);
            }
        }
        for (Player p : player) {
            if (!showBothMaps) {
                if (p.getFloor() == 0)
                    p.render(g);
            }
            else {
                if (p.getFloor() != 0)
                    p.render(g);
            }
        }
        if(debugON) { //display debug information
            debugModeOn(g, player);
        }

        for(Item i : player[playerID].inventory.items) {
            if(i != null) {
                i.render(g);
            }
        }
        for (Projectile proj : projectiles) {
            if (!showBothMaps) {
                if (proj.getFloor() == 0)
                    proj.render(g);
            }
            else {
                if(proj.getFloor() != 0)
                    proj.render(g);
            }
        }

        // HUD information
        player[playerID].inventory.render(g);
        player[playerID].healthbar.render(g);
        player[playerID].meter.render(g);
        g.drawImage(keyImage, player[playerID].getX() - 380, player[playerID].getY() - 270);
        g.drawString("" + player[playerID].getKeyCount(), player[playerID].getX() - 345, player[playerID].getY() - 265);   
        g.drawImage(coinImage, player[playerID].getX() - 320, player[playerID].getY() - 268);
        g.drawString("" + player[playerID].getScore(), player[playerID].getX() - 295, player[playerID].getY() - 265);
    }


    public void collides(int delta) {
        /** check player collisions */
        for (Player p : player) {
            ArrayList<Tile> nearTiles = map_floor[p.getFloor()].nearTiles(p.getPosition());
            if (nearTiles != null)
                for (Tile t : nearTiles)
                    t.handleCollision(p);
            for (Projectile proj : projectiles) // pvp projectiles
                if (p.collides(proj) != null)
                    p.removeHealth(proj.getAttack());
            for (Player pOther : player) { // pvp melee
                if (pOther == p)
                    continue;
                if (p.getFloor() == pOther.getFloor() && p.collides(pOther) != null)
                    if (p.isAttacking() == true) {
                        pOther.removeHealth(p.getAttack());
                		if(pOther.isDead()) {							//player p killed pOther
                			p.addPlayerScore(pOther.getScore() / 2);	//player takes half of pOther score
                			pOther.reducePlayerScore();					//reduce pOther score by half
                		}
                    }
            }
        }
        /** check spawner collisions */
        for (Spawner s : spawners) {
            for (Projectile proj : projectiles) {
                if(proj.getFloor() == s.getFloor()) {
                	if (s.collides(proj) != null) {
                		if(s.removeHealth(proj.getAttack())) {
                			player[wizardNum].addPlayerScore(spawnerScore);	//increment wizard score
                		}
                    }
                }
            }
            for (Player p : player) {
                if(s.getFloor() == p.getFloor()) {
                	if (s.collides(p) != null) {
	                    if (p.isAttacking() == true) {
	                        if(s.removeHealth(p.getAttack())) {
	                        	p.addPlayerScore(spawnerScore);
	                        }
	                    }
	                }
                }
            }
            for (Enemy e : s.getEnemies()) {
                ArrayList<Tile> nearTiles = map_floor[e.getFloor()].nearTiles(e.getPosition());
                if (nearTiles != null)
                    for (Tile t : nearTiles)
                        t.handleEnemyCollision(e);
                for (Projectile proj : projectiles) {
	                if(e.getFloor() == proj.getFloor()) {
	                    if (e.collides(proj) != null) {
	                        if(e.removeHealth(proj.getAttack())) {
	                        	player[wizardNum].addPlayerScore(enemyScore);
	                        }
	                    }
	                }
                }
                for (Player p : player) {
                    if(e.getFloor() == p.getFloor()) {
                    	if (e.collides(p) != null) {	                    
	                        if (e.isAttacking() == true)
	                            p.removeHealth(e.getAttack());
	                        else
	                            e.attack();
	                        if (p.isAttacking() == true) {
	                            if(e.removeHealth(p.getAttack())) {
	                            	p.addPlayerScore(enemyScore);
	                            }
	                        }
                    	}
                    }
                }
            }
        }

        /** check enemy collisions */
        for (Enemy e : enemies) {
        	 ArrayList<Tile> nearTiles = map_floor[e.getFloor()].nearTiles(e.getPosition());
             if (nearTiles != null)
                 for (Tile t : nearTiles)
                     t.handleEnemyCollision(e);
             for (Projectile proj : projectiles) {
	                if(e.getFloor() == proj.getFloor()) {
	                    if (e.collides(proj) != null) {
	                        if(e.removeHealth(proj.getAttack())) {
	                        	player[wizardNum].addPlayerScore(enemyScore);
	                        }
	                    }
	                }
             }
             for (Player p : player) {
                 if(e.getFloor() == p.getFloor()) {
                 	if (e.collides(p) != null) {	                    
	                        if (e.isAttacking() == true)
	                            p.removeHealth(e.getAttack());
	                        else
	                            e.attack();
	                        if (p.isAttacking() == true) {
	                            if(e.removeHealth(p.getAttack())) {
	                            	p.addPlayerScore(enemyScore);
	                            }
	                        }
                 	}
                 }
             }
        }
    }

    public static LinkedList<Spawner> getSpawners() {
    		return spawners;
    }
    
    public static LinkedList<Enemy> getEnemies() {
		return enemies;
    }
    
    public static void setGameOver() {
        if (Gauntlet.isHost == true)
            msgInQueue.add(new GameOverMessage());
        else
            msgOutQueue.add(new GameOverMessage());
    }

    public void gameOver(StateBasedGame game) {
        game.enterState(Gauntlet.GAMEOVERSTATE);
        for(int i = 0; i < 4; i++)
            GameOverState.updateScore(i, player[i].getScore());
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        m = map_floor[player[playerID].getFloor()];
        LinkedList<Tile> removeUnlockables = new LinkedList<Tile>();
        for (Tile t : unlockables) {
            t.update(delta);
            if (t.isUnlockable() == false)
                removeUnlockables.add(t);
        }
        unlockables.removeAll(removeUnlockables);

        // Creates a list of dead enemies and removes them from the dungeon
        LinkedList<Enemy> deadEnemies = new LinkedList<Enemy>();
        for (Enemy e : enemies) {
            if (e == null) break;
            if (e.isDead() == true) {
                deadEnemies.add(e);
            }
            e.update(delta, player);
        }
        enemies.removeAll(deadEnemies);
        for (int i = 0; i < enemies.size(); i++) {
            Enemy e1 = enemies.get(i);
            if (e1 == null) continue;
            for (int j = i + 1; j < enemies.size(); j++) {
                Enemy e2 = enemies.get(j);
                if (e2 == null) continue;
                if (e1.collides(e2) != null) {
                    e2.moveBack();
                }
            }
        }

        LinkedList<Spawner> deadSpawners = new LinkedList<Spawner>();
        for (Spawner s : spawners) {
            s.update(delta, player);
            if (s.isDead() == true) {
                enemies.addAll(s.getEnemies());
                deadSpawners.add(s);
            }
        }
        spawners.removeAll(deadSpawners);

        LinkedList<Projectile> deadProjectiles = new LinkedList<Projectile>();
        for (Projectile proj : projectiles) {
            proj.update(delta);
            if (proj.isDead() == true)
                deadProjectiles.add(proj);
        }
        projectiles.removeAll(deadProjectiles);

        for (Player p : player) {
            p.update(delta, player);
        }

        /** send input */
        InputMessage msg = new InputMessage(playerID);
        boolean moved = false;
        if (player[playerID].getHealth() > 0) {
            if (input.isKeyDown(Input.KEY_UP)) {
                msg.up = true;
                moved = true;
            }
            if (input.isKeyDown(Input.KEY_DOWN)) {
                msg.down = true;
                moved = true;
            }
            if (input.isKeyDown(Input.KEY_LEFT)) {
                msg.left = true;
                moved = true;
            }
            if (input.isKeyDown(Input.KEY_RIGHT)) {
                msg.right = true;
                moved = true;
            }
            if (input.isKeyPressed(Input.KEY_SPACE)) {
                msg.attack = true;
                moved = true;
            }
            if (input.isKeyPressed(Input.KEY_D)) {
                System.out.println(enemies.size());
                msg.dead = true;
                moved = true;
            }
            if (input.isKeyPressed(Input.KEY_ESCAPE)) {
                //gameOver(game);
                if (Gauntlet.isHost == true)
                    msgInQueue.add(new GameOverMessage());
                else
                    msgOutQueue.add(new GameOverMessage());
            }
            if(input.isKeyDown(Input.KEY_1)) {
                player[playerID].useItem(0);
            }
            if(input.isKeyDown(Input.KEY_2)) {
                player[playerID].useItem(1);
            }
            if(input.isKeyDown(Input.KEY_3)) {
                player[playerID].useItem(2);
            }
        }


        /* get input from other players */
        if (moved == true) {
            msgInQueue.add(msg);
            //if (Gauntlet.isHost == false)
                msgOutQueue.add(msg);
        }
        if (Gauntlet.isHost == false) {
            if (player[playerID].isTouched() == true) {
                msgOutQueue.add(new PlayerMessage(playerID, player[playerID]));
            }
        }


        /** handle all input */
        handleMsgQueue(delta, game);
        if (msgOutQueue.peek() != null)
            new Client().start();
        if (Gauntlet.isHost == true) {
            writeMsgs();
        }
        collides(delta);
        viewport.setPosition(player[playerID].getPosition()); // keep the view centered on the cursor

        player[playerID].inventory.update(player[playerID].getPosition());
        player[playerID].healthbar.update(player[playerID].getPosition());
        player[playerID].meter.update(player[playerID].getPosition());
    }

    private void writeMsgs() {
        for (int i = 0; i < player.length; i++)
            if (player[i].isTouched() == true)
                msgOutQueue.add(new PlayerMessage(i, player[i]));
    }

    private void handleMsgQueue(int delta, StateBasedGame game) {
        while (msgInQueue.peek() != null) {
            Message msg = (Message) msgInQueue.poll();
            switch (msg.getType()) {
                case INPUT:
                    InputMessage inputMsg = (InputMessage) msg;
                    inputMsg.updatePlayer(player, delta);
                    break;
                case PLAYER:
                    PlayerMessage playerMsg = (PlayerMessage) msg;
                    playerMsg.updatePlayer(player);
                    break;
                case ENEMY:
                    EnemyMessage enemyMsg = (EnemyMessage) msg;
                    break;
                case TILE:
                    TileMessage tileMsg = (TileMessage) msg;
                    break;
                case GAMEOVER:
                    if (Gauntlet.isHost == true)
                        msgOutQueue.add(msg);
                    gameOver(game);
                    break;
            }
        }
    }

    @Override
    public int getID() {
        return Gauntlet.PLAYINGSTATE;
    }
    
    public void debugModeOn(Graphics g, Player[] player) {
    	g.drawString("Health = " + player[playerID].getHealth(), player[playerID].getX() + 250, player[playerID].getY() - 300);
        g.drawString("Keys = " + player[playerID].getKeyCount(), player[playerID].getX() + 250, player[playerID].getY() - 280);
        g.drawString("Items = " + player[playerID].inventory.numberOfItems, player[playerID].getX() + 250, player[playerID].getY() - 260);
        g.drawString("MAX = " + player[playerID].getMaxHealth(), player[playerID].getX() + 250, player[playerID].getY() - 240);
        if(player[playerID].inventory.items[0] != null) {
            g.drawString("Items = " + player[playerID].inventory.items[0].itemNum, player[playerID].getX() + 250, player[playerID].getY() - 220);
        }
        if(player[playerID].inventory.items[1] != null) {
            g.drawString("Items = " + player[playerID].inventory.items[1].itemNum, player[playerID].getX() + 250, player[playerID].getY() - 200);
        }
        if(player[playerID].inventory.items[2] != null) {
            g.drawString("Items = " + player[playerID].inventory.items[2].itemNum, player[playerID].getX() + 250, player[playerID].getY() - 180);
        }
        if(player[playerID].powerUp != null) {
            g.drawString("Power up = " + player[playerID].powerUp.itemNum, player[playerID].getX() + 250, player[playerID].getY() - 140);
        }
        g.drawString("Floor = " + player[playerID].getFloor(), player[playerID].getX() + 250, player[playerID].getY() - 160);
        g.drawString("Attack = " + player[playerID].getAttack(), player[playerID].getX() + 250, player[playerID].getY() - 120);
        g.drawString("Is a wizard? = " + player[playerID].isWizard(player[playerID]), player[playerID].getX() + 250, player[playerID].getY() - 100);
        g.drawString("Score = " + player[playerID].getScore(), player[playerID].getX() + 250, player[playerID].getY() - 80);
    }
}
