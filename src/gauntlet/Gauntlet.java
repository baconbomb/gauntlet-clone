package gauntlet;

import jig.ResourceManager;
import jig.Entity;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Client Skeleton Code.
 *
 * Just enough code to compile a running "game" that displays an empty window.
 *
 */
public class Gauntlet extends StateBasedGame {
    private static final String GAME_TITLE = "Gauntlet";

    public static final int MENUSTATE = 0;
    public static final int PLAYINGSTATE = 1;
    public static final int LEVELEDITOR = 2;
    public static final int GAMEOVERSTATE = 3;
    public static final int LOBBYSTATE = 4;
    public static final int TITLESTATE = 5;

    public static final int WINDOW_HEIGHT = 600;
    public static final int WINDOW_WIDTH = 800;

    public static final String DUNGEON = "gauntlet/resources/images/dungeon_tiles.png";
    public static final String ITEMS = "gauntlet/resources/images/dungeon_items.png";
    public static final String COIN = "gauntlet/resources/images/coin.png";
    public static final String BUBBLE = "gauntlet/resources/images/bubble.png";
    public static final String CURSOR = "gauntlet/resources/images/cursor.png";
    public static final String BG = "gauntlet/resources/images/background.png";
    public static final String BG2 = "gauntlet/resources/images/background2.png";
    public static final String ADVENTURER = "gauntlet/resources/images/Adventurer_Sheet.png";
    public static final String WARRIOR = "gauntlet/resources/images/Dwarf_Sheet.png";
    public static final String GLADIATOR = "gauntlet/resources/images/Gladiator_Sheet.png";
    public static final String WIZARD = "gauntlet/resources/images/Wizard_Sheet.png";
    public static final String WORM = "gauntlet/resources/images/Worm_Sheet.png";
    public static final String POTIONS = "gauntlet/resources/images/potion_sprites.png";
    public static final String PROJECTILE = "gauntlet/resources/images/projectile_sheet.png";
    public static final String SKELETON = "gauntlet/resources/images/Skeleton_Sheet.png";
    public static final String SNAKE = "gauntlet/resources/images/Snake_Sheet.png";
    public static final String GOLEM = "gauntlet/resources/images/Golem_Sheet.png";
    public static final String INVENTORY_BACKGROUND = "gauntlet/resources/images/inventory.png";
    public static final String HEALTH_EMPTY = "gauntlet/resources/images/health_meter.png";
    public static final String HEALTH_1 = "gauntlet/resources/images/health_meter1.png";
    public static final String HEALTH_2 = "gauntlet/resources/images/health_meter2.png";
    public static final String HEALTH_3 = "gauntlet/resources/images/health_meter3.png";
    public static final String HEALTH_4 = "gauntlet/resources/images/health_meter4.png";
    public static final String HEALTH_5 = "gauntlet/resources/images/health_meter5.png";
    public static final String HEALTH_6 = "gauntlet/resources/images/health_meter6.png";
    public static final String HEALTH_7 = "gauntlet/resources/images/health_meter7.png";
    public static final String HEALTH_8 = "gauntlet/resources/images/health_meter8.png";
    public static final String HEALTH_9 = "gauntlet/resources/images/health_meter9.png";
    public static final String HEALTH_10 = "gauntlet/resources/images/health_meter10.png";

    public static final String POTION_METER = "gauntlet/resources/images/potion_meter_empty.png";
    public static final String POTION_METER_BLUE = "gauntlet/resources/images/potion_meter_b.png";
    public static final String POTION_METER_GREEN = "gauntlet/resources/images/potion_meter_g.png";
    public static final String POTION_METER_RED = "gauntlet/resources/images/potion_meter_r.png";
    public static final String POTION_METER_YELLOW = "gauntlet/resources/images/potion_meter_y.png";
    
    public static final String CURSOR_IMAGE_1 = "gauntlet/resources/images/cursor_1.png";
    public static final String CURSOR_IMAGE_2 = "gauntlet/resources/images/cursor_2.png";
    public static final String CURSOR_IMAGE_3 = "gauntlet/resources/images/cursor_3.png";
    public static final String CURSOR_IMAGE_4 = "gauntlet/resources/images/cursor_4.png";
    public static final String CURSOR_READY = "gauntlet/resources/images/ready_icon.png";
    
    public static final String ENTER_BUTTON = "gauntlet/resources/images/enter_button.png";
    public static final String WAITING_BUTTON = "gauntlet/resources/images/waiting_button.png";
    public static final String MENU_BUTTON = "gauntlet/resources/images/menu_button.png";
    public static final String CLIENT_BUTTON = "gauntlet/resources/images/client_button.png";
    public static final String CLIENT_BUTTON_C = "gauntlet/resources/images/client_button_c.png";
    public static final String HOST_BUTTON = "gauntlet/resources/images/host_button.png";
    public static final String HOST_BUTTON_C = "gauntlet/resources/images/host_button_c.png";
    public static final String LOBBY_BANNER = "gauntlet/resources/images/select_banner.png";
    public static final String GAMEOVER_BANNER = "gauntlet/resources/images/gameover_banner.png";
    public static final String TITLE_BANNER = "gauntlet/resources/images/title_banner.png";
    public static final String IP_BANNER = "gauntlet/resources/images/ip_banner.png";
    public static final String FAILED_BANNER = "gauntlet/resources/images/failed_banner.png";
    public static final String IP_INPUT = "gauntlet/resources/images/ip_input.png";
    public static final String MESSAGE_BOX = "gauntlet/resources/images/message_box.png";
    
    public static final String ADVENTURER_STATS = "gauntlet/resources/images/stats_adventurer.png";
    public static final String WARRIOR_STATS = "gauntlet/resources/images/stats_warrior.png";
    public static final String WIZARD_STATS = "gauntlet/resources/images/stats_wizard.png";
    public static final String GLADIATOR_STATS = "gauntlet/resources/images/stats_gladiator.png";

    public static final String MAP1_ROOMS = "gauntlet/resources/maps/0.map";
    public static final String MAP1_LOWER = "gauntlet/resources/maps/1.map";
    public static final String MAP1_UPPER = "gauntlet/resources/maps/2.map";

    public static Input input;

    public static boolean isHost;
    public static Server server;
    public static Queue<Message> msgInQueue;
    public static Queue<Message> msgOutQueue;

    public Gauntlet(String title) {
        super(title);
    }

    public static void startServer() {
        if (server == null) {
            server = new Server();
            server.start();
        }
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        Entity.setCoarseGrainedCollisionBoundary(Entity.AABB);
        ResourceManager.loadImage(DUNGEON);
        ResourceManager.loadImage(ITEMS);
        ResourceManager.loadImage(CURSOR);
        ResourceManager.loadImage(ADVENTURER);
        ResourceManager.loadImage(WARRIOR);
        ResourceManager.loadImage(GLADIATOR);
        ResourceManager.loadImage(WIZARD);
        ResourceManager.loadImage(WORM);
        ResourceManager.loadImage(POTIONS);
        ResourceManager.loadImage(BG);
        ResourceManager.loadImage(BG2);
        ResourceManager.loadImage(PROJECTILE);
        ResourceManager.loadImage(SKELETON);
        ResourceManager.loadImage(SNAKE);
        ResourceManager.loadImage(GOLEM);
        ResourceManager.loadImage(INVENTORY_BACKGROUND);
        ResourceManager.loadImage(HEALTH_EMPTY);
        ResourceManager.loadImage(HEALTH_1);
        ResourceManager.loadImage(HEALTH_2);
        ResourceManager.loadImage(HEALTH_3);
        ResourceManager.loadImage(HEALTH_4);
        ResourceManager.loadImage(HEALTH_5);
        ResourceManager.loadImage(HEALTH_6);
        ResourceManager.loadImage(HEALTH_7);
        ResourceManager.loadImage(HEALTH_8);
        ResourceManager.loadImage(HEALTH_9);
        ResourceManager.loadImage(HEALTH_10);
        ResourceManager.loadImage(POTION_METER);
        ResourceManager.loadImage(POTION_METER_RED);
        ResourceManager.loadImage(POTION_METER_YELLOW);
        ResourceManager.loadImage(POTION_METER_GREEN);
        ResourceManager.loadImage(POTION_METER_BLUE);
        ResourceManager.loadImage(CURSOR_IMAGE_1);
        ResourceManager.loadImage(CURSOR_IMAGE_2);
        ResourceManager.loadImage(CURSOR_IMAGE_3);
        ResourceManager.loadImage(CURSOR_IMAGE_4);
        ResourceManager.loadImage(CURSOR_READY);
        ResourceManager.loadImage(ADVENTURER_STATS);
        ResourceManager.loadImage(WARRIOR_STATS);
        ResourceManager.loadImage(WIZARD_STATS);
        ResourceManager.loadImage(GLADIATOR_STATS);
        ResourceManager.loadImage(ENTER_BUTTON);
        ResourceManager.loadImage(WAITING_BUTTON);
        ResourceManager.loadImage(MENU_BUTTON);
        ResourceManager.loadImage(HOST_BUTTON);
        ResourceManager.loadImage(HOST_BUTTON_C);
        ResourceManager.loadImage(CLIENT_BUTTON);
        ResourceManager.loadImage(CLIENT_BUTTON_C);
        ResourceManager.loadImage(LOBBY_BANNER);
        ResourceManager.loadImage(GAMEOVER_BANNER);
        ResourceManager.loadImage(TITLE_BANNER);
        ResourceManager.loadImage(IP_BANNER);
        ResourceManager.loadImage(FAILED_BANNER);
        ResourceManager.loadImage(IP_INPUT);
        ResourceManager.loadImage(MESSAGE_BOX);
        ResourceManager.loadImage(COIN);
        ResourceManager.loadImage(BUBBLE);

        input = container.getInput();
        isHost = false;
        msgInQueue = new LinkedList<Message>();
        msgOutQueue = new LinkedList<Message>();
        addState(new TitleState());
        //addState(new MenuState());
        addState(new LobbyState());
        addState(new PlayingState());
        addState(new LevelEditor());
        addState(new GameOverState());
    }

    public static void main(String[] args) {
        AppGameContainer app;
        try {
            app = new AppGameContainer(new Gauntlet(GAME_TITLE));
            app.setDisplayMode(WINDOW_WIDTH, WINDOW_HEIGHT, false);
            app.setVSync(true);
            app.setAlwaysRender(true);
            app.setUpdateOnlyWhenVisible(false);
            app.setShowFPS(false);
            app.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }

    }

}
