package gauntlet;

import org.newdawn.slick.Animation;
import jig.Vector;

public class Worm extends Enemy {
	private static final int HEALTH = 20;
	private static final int ATTACK = 5;
	private static final int SPEED = 2;
	private static final int ATTACK_SPEED = 2;
	
	public Worm(Vector position, int floor) {
		super(position, HEALTH, ATTACK, ATTACK_SPEED, SPEED, floor);
	}

}
