package gauntlet;

public class InputMessage extends Message {
    int playerID;
    boolean left;
    boolean right;
    boolean up;
    boolean down;
    boolean attack;
    boolean dead;

    public InputMessage(int id) {
        super(MessageType.INPUT);
        playerID = id;
        left = false;
        right = false;
        up = false;
        down = false;
        attack = false;
        dead = false;
    }

    public void updatePlayer(Player[] player, int delta) {
        Player p = player[playerID];
        if (p != null) {
            if (up == true)
                p.moveUp(delta);
            if (down == true)
                p.moveDown(delta);
            if (left == true)
                p.moveLeft(delta);
            if (right == true)
                p.moveRight(delta);
            if (attack == true)
                p.attack();
            if (dead == true)
                p.death();
        }
    }
}
