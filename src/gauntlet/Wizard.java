package gauntlet;

import org.newdawn.slick.Animation;

import jig.ResourceManager;
import jig.Vector;

public class Wizard extends Player {
    private static final int HEALTH = 150;
    private static final int ATTACK = 20;
    private static final double DEFENSE = 0.02;
    private static final int SPEED = 3;
    private static final int ATTACK_SPEED = 3;

    public Wizard(Vector position) {
        this(position, true);
    }

    public Wizard(Vector position, boolean selected) {
        super(position, HEALTH, ATTACK, DEFENSE, SPEED, ATTACK_SPEED, playerType.WIZARD, selected);

        right_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 0, 7, 0, true, 180, true);
        right_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 1, 10, 1, true, 100, true);
        right_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 2, 4, 2, true, 50, true);
        right_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 3, 8, 3, true, 100, true);
        right_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 128, 64), 0, 4, 3, 4, true, 100, true);

        left_idle = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 5, 7, 5, true, 180, true);
        left_walk = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 6, 10, 6, true, 100, true);
        left_damage = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 7, 4, 7, true, 50, true);
        left_hurt = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 64, 64), 0, 8, 8, 8, true, 100, true);
        left_death = new Animation(ResourceManager.getSpriteSheet(Gauntlet.WIZARD, 128, 64), 0, 9, 3, 9, true, 100, true);

        right_damage.setLooping(false);
        left_damage.setLooping(false);
        right_damage.stop();
        left_damage.stop();

        right_death.setLooping(false);
        left_death.setLooping(false);
        right_death.stop();
        left_death.stop();
        
        right_hurt.setLooping(false);
        left_hurt.setLooping(false);
        right_hurt.stop();
        left_hurt.stop();

        current = right_idle;
        addAnimation(current);
    }

    @Override
    public void attack() {
        if (attackTimer <= 0) {
            setIdle(false);
            attackTimer = (int) (3000 / getAttackSpeed());
            updateAnimation(right_damage, left_damage);
            // spawn projectile
            Vector a = new Vector(this.getX(), this.getY() + 15);
            PlayingState.addProjectile(new Projectile(a, isRight(), false, getAttack(), getFloor()));
        }
    }
}
