package gauntlet;

import jig.ResourceManager;
import jig.Entity;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


class LevelEditorMenu extends BasicGameState {
    private Viewport viewport;
    private int cursor;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        input = Gauntlet.input;
        cursor = 0;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame game) {
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        // draw menu box
        // draw option
        // highlight selection
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if (input.isKeyPressed(Input.KEY_ENTER)) {
            switch (cursorPos) {
                case 0:
                    game.enterState(LevelEditorMenu.getID());
                case 1:
            }
        }
    }

    /** Moves cursor image up */
    private void cursorUp() {
        if (cursorPos[1] > m.getMinY()) {
            cursorPos[1]--;
            cursor.translate(0, -TILE);
        }
    }

    /** Move cursor image up */
    private void cursorDown() {
        if (cursorPos[1] < m.getMaxY()-1) {
            cursorPos[1]++;
            cursor.translate(0, TILE);
        }
    }

    @Override
    public int getID() {
        return Gauntlet.LEVEL_EDITOR_MENU_ID;
    }
}
